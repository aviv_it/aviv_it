﻿using AVI_IT.Controllers;
using AVI_IT.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace AVIV_UnitTesting
{
    class ProductTypeControllerTest
    {
        private readonly AvivDataContext _db;

        [SetUp]
        public void Setup()
        {

        }
        public ProductTypeControllerTest()
        {

        }

        public ProductTypeControllerTest(AvivDataContext db)
        {
            _db = db;

        }
        //Testing Product Type

        [Test]
        public void AddProductType_ReturnTrue()
        {
            var controller = new ProductTypesController(_db);

            ProductType proType = new ProductType()
            {
                typeID = 200,
                bType = true,
                type = "HardWare"
            };
            var result = controller.Create(proType);

            Assert.IsNotNull(result);
            
        }

        [Test]
        public void EditProduct_ReturnTrue()
        {
            var controller = new ProductTypesController(_db);

            ProductType proType = new ProductType()
            {
                typeID = 3,
                bType = true,
                type = "Cat"
            };
            var result = controller.Edit(proType);

            Assert.IsNotNull(result);

        }

        [Test]
        public void DeleteProduct_ReturnTrue()
        {
            var controller = new ProductTypesController(_db);

            var prod = ProdType();
            var result = controller.Delete(3, prod);

            Assert.IsNotNull(result);

        }

        public ProductType ProdType()
        {
            ProductType proType = new ProductType()
            {
                typeID = 3,
                bType = true,
                type = "Cat"
            };

            return proType;
        }


    }
}
