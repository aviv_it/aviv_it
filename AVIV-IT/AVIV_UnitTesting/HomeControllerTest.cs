using NUnit.Framework;
using AVI_IT.Controllers;
using AVI_IT.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Net;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using AVI_IT.ViewModels;
using Microsoft.AspNetCore.Hosting;
using AVI_IT.Services;

namespace AVIV_UnitTesting
{
   
    public class Tests
    {
        private readonly AvivDataContext _db;
      
        [SetUp]
        public void Setup()
        {
            
        }
        public Tests()
        {
            
        }
        
        public Tests(AvivDataContext db)
        {
            _db = db;
            
        }

        [Test]
        public void ReturnHomePage()
        {
            HomeController controller = new HomeController(_db);
            ViewResult result = controller.Home() as ViewResult;
            Assert.IsNotNull(result);
          
        }

        //Testing Service Page

        [Test]
        public void ReturnAddServicePage()
        {
            var controller = new HomeController(_db);
           
            var result = controller.AddService();
            Assert.IsNotNull(result); 
            
        }

        [Test]
        public void EditService_ReturnNewService()
        {
            var controller = new HomeController(_db);
            Service s;
            s = new Service()
            {
                serviceID = 115,
                serviceTitle = "Data Recovery",
                serviceContent = "Help all enquries regarding recover all the data."
            };

            var result = controller.EditService(s);

            Assert.IsNotNull(result);


        }

      



    }
}