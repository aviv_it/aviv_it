using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AVI_IT.Models;
using AVI_IT.ViewModels;
using DNTCaptcha.Core;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using NLog;
using Stripe;
using Wkhtmltopdf.NetCore;

namespace global
{
    public static class gLogger
    {
        public static Logger  log = null;
    }
}
namespace AVI_IT
{
    public class Startup
    {

        private readonly IConfigurationRoot configuration;
        private readonly IConfiguration con;

        public Startup(IWebHostEnvironment env, IConfiguration con)
        {
            configuration = new ConfigurationBuilder()
                .AddEnvironmentVariables().AddJsonFile(env.ContentRootPath + "/config.json")
                .Build();
            global.gLogger.log = LogManager.GetCurrentClassLogger();
            this.con = con;
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
          

            services.AddIdentity<IdentityUserHelper, IdentityRole>()

                .AddEntityFrameworkStores<AvivDataContext>()
                .AddDefaultTokenProviders();

            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = new PathString("/Account/Login");
                options.AccessDeniedPath = new PathString("/Account/Denied");
                options.LogoutPath = new PathString("/Account/Login");
            });
            services.AddMvc();
            services.AddAuthentication().AddGoogle(options =>
            {
                options.ClientId = con.GetSection("Google").GetSection("GoggleClientId").Value;
                options.ClientSecret = con.GetSection("Google").GetSection("GoogleClientSecret").Value;
            });
            services.AddAuthentication().AddFacebook(options =>
            {
                options.ClientId = con.GetSection("Facebook").GetSection("FacebookClientId").Value;
                options.ClientSecret = con.GetSection("Facebook").GetSection("FacebookClientId").Value;
            });
            services.AddSession(options =>
            {
                //options.Cookie.Name = ".AdventureWorks.Session";
                options.IdleTimeout = TimeSpan.FromMinutes(30);
                options.Cookie.IsEssential = true;
            });
            services.AddDNTCaptcha(options => options
           .UseCookieStorageProvider()
           .ShowThousandsSeparators(false)
           .WithEncryptionKey("This is my secure key!"));
            
/*            services.AddDbContext<AvivDataContext>(options =>
            {
                var connectionString = configuration.GetConnectionString("PostDataContext");
                options.UseMySql(connectionString);
            });
*/
            services.AddDbContext<AvivDataContext>(options =>
            {
                var connectionString = configuration.GetConnectionString("PostDataContext");
                // options.UseSqlServer(connectionString);
                // options.UseMySql(connectionString);
                options.UseMySql(
                    connectionString, b => b.MigrationsAssembly(typeof(AvivDataContext).Assembly.FullName));
            });
            //services.AddTransient<AvivDataContext>();
            services.AddScoped<AvivDataContext>();
            //services.AddIdentity< AVI_IT.ViewModels.IdentityUserHelper, IdentityRole>().AddDefaultTokenProviders();

            //services.AddScoped<Employee>();

          

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IConfiguration con)
        {
            Startup config = new Startup(env, con);
            //Startup configPayment = new Startup(con);

            if (config.configuration.GetValue<bool>("EnableDeveloperExceptions"))
            {
                app.UseDeveloperExceptionPage();
            }
            else
                app.UseExceptionHandler("/error.html");

            app.Use(async (context, next) =>
            {
                
                
                await next();
            
            });
            string secretKey = config.con.GetValue<string>("Stripe:SecretKey");
            StripeConfiguration.ApiKey = secretKey;

            app.UseRouting();
            
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseSession();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute("default", "{controller=Home}/{action=Home}/{id?}");

            });

            app.UseFileServer();
            
        }
    }
}
