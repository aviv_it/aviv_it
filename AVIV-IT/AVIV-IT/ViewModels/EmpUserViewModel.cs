﻿using AVI_IT.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.ViewModels
{
    public class EmpUserViewModel
    {
        public Employee employee { get; set; }
        public IdentityUserHelper user {get;set;}
        public Quotation quotation { get; set; }
        public HelpDeskViewModel ticket { get; set; }

    }
}
