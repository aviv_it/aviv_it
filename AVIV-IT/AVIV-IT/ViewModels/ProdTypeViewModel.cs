﻿using AVI_IT.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.ViewModels
{
    public class ProdTypeViewModel
    {
        public Product product { get; set; }
        public ProductType pType { get; set; }

    }
}
