﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.ViewModels
{
    public class IdentityUserHelper : IdentityUser
    {
        [Display(Name ="FirstName")]
        public string firstName { get; set; }
        [Display(Name = "LastName")]
        public string lastName { get; set; }
        [Display(Name = "ConfirmPassword")]
        public string confirmPassword { get; set; }
        [Display(Name = "AddressLine1")]
        public string addressLine1 { get; set; }
        [Display(Name = "Suburb")]
        public string suburb { get; set; }
        [Display(Name = "Postcode")]
        public int postcode { get; set; }
        [Display(Name = "State")]
        public string state { get; set; }
        [Display(Name = "Country")]
        public string country { get; set; }


        

    }
}
