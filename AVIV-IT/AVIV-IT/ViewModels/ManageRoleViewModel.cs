﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.ViewModels
{
    public class ManageRoleViewModel
    {
        public string userID { get; set; }

        public string roleID { get; set; }
        public List<SelectListItem> userList { get; set; }

        public List<SelectListItem> roleList { get; set; }
    }
}
