﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.ViewModels
{
    public class HelpDeskViewModel
    {
        [Key]
        [Display(Name = "TicketID")]
        public int ticketID { get; set; }
        
        [Display(Name ="Services ")]
        [Required(ErrorMessage = "Category is required")]
        public string category { get; set; }
        
        [Display(Name = "Description ")]
        [Required(ErrorMessage = "Description is required")]
        public string ticketDescription { get; set; }

        public string userID { get; set; }
        public int? empID { get; set; }

        [NotMapped]
        public List<SelectListItem> categoryList { get; set; }
        [NotMapped]
        [Display(Name = "Assigned Employee ")]
        public string empName { get; set; }

        public HelpDeskViewModel()
        {
            categoryList = new List<SelectListItem>();
        }
    }
}
