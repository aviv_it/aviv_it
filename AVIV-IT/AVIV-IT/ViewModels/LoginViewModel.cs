﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "User Name is Required")]
        [EmailAddress]
        public string Email { get; set; }
        [Required(ErrorMessage = "Password is Required")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Display(Name = "Remember Me")]
        public bool RememberMe { get; set; }
        public string ReturnUrl { get; set; }
        public IList<AuthenticationScheme> ExternalLogins { get; set; }
        [NotMapped]
        public string flag { get; set; }
        /*
                private readonly IConfiguration _configuration;
                public LoginViewModel(IConfiguration configuration)
                {
                    _configuration = configuration;
                }
                */


    }
}
