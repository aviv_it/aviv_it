﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.ViewModels
{
    public class RegisterViewModel
    {

        [Display(Name = "First Name")]
        [Required(ErrorMessage = "First Name is Required")]
        [RegularExpression("^[A-Za-z,.'-]{2,}", ErrorMessage = "First name should be at least 2 letters long, with no special characters")]
        public string firstName { get; set; }
        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Last Name is Required")]
        [RegularExpression("^[A-Za-z,.'-]{2,}", ErrorMessage = "Last name should be at least 2 letters long, with no special characters")]
        public string lastName { get; set; }

        [Display(Name = "Phone Number", Prompt = "0455555555")]
        [Required(ErrorMessage = "Phone Number is Required")]
        [Phone]
        [MinLength(10, ErrorMessage = "Phone number must be 10 digits")]
        [MaxLength(10, ErrorMessage = "Phone number must be 10 digits")]
        public string phone { get; set; }
        [Display(Name = "Email", Prompt = "example@example.com")]
        [Required(ErrorMessage = "Email is Required")]
        [RegularExpression(@"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z",
                            ErrorMessage = "Please enter a valid email address")]
        public string email { get; set; }

        [Display(Name = "Password")]
        [Required]
        [DataType(DataType.Password)]
        public string password { get; set; }

        [Display(Name = "Confirm Password")]
        [Required]
        [DataType(DataType.Password)]
        [Compare("password", ErrorMessage = "Password and confirm password do not match")]
        public string confirmPassword { get; set; }

        [Display(Name = "Address ")]
        [Required(ErrorMessage = "Address is Required")]
        public string addressLine1 { get; set; }
        [Display(Name = "Suburb")]
        [Required(ErrorMessage = "Suburb is Required")]
        public string suburb { get; set; }
        [Display(Name = "Postcode")]
        [Required(ErrorMessage = "Postcode is Required")]
        /*[MaxLength(4, ErrorMessage = "Postcode must be m 4digits ")]
        [MinLength(4, ErrorMessage = "Postcode must be 4digits ")]*/
        public int? postcode { get; set; }
        [Display(Name = "State")]
        [Required(ErrorMessage = "State is Required")]
        public string state { get; set; }
        [Display(Name = "Country")]
        [Required(ErrorMessage = "Country is Required")]
        public string country { get; set; }
        [NotMapped]
        [Required(ErrorMessage = "You must read and accept Privacy Policy")]
        public bool IsChecked { get; set; }

    }
}
 