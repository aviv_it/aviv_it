﻿using AVI_IT.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.ViewModels
{
   
    public class ShipingDetailViewModel
    {
        public string selectedVariant { get; set; }
        public double totalCartPrice { get; set; }

        public double grandTotal { get; set; }
        [NotMapped]

        public IdentityUserHelper user { get; set;}
        [NotMapped]
        public List<ProductCart> cartList { get; set; }
        [NotMapped]
        public CartModel cartUser { get; set; }
        [NotMapped]
        public List<Product> productList { get; set; }
        [NotMapped]
        public List<string> shippingVariant { get; set; }
        [NotMapped]
        public Keys keys { get; set; }
        [NotMapped]
        public AddressModel address { get; set; }

        [NotMapped]
        public bool sameAd { get; set; }

       
    }
}
