﻿using AVI_IT.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.ViewModels
{
    public class BlogViewModel
    {
        [Key]
        public int blogID { get; set; }

        public string blogTitle { get; set; }
        public string blogContent { get; set; }
        public string userID { get; set; }

        public DateTime publishedDate { get; set; }

        [NotMapped]
        public string firstName { get; set; }
        [NotMapped]
        public string lastName { get; set; }
        [NotMapped]
        //Wrap CommentModel in this model, so we can use two models in razor view
        public IEnumerable<LeaveCommentViewModel> Comments { get; set; }

    }
}
