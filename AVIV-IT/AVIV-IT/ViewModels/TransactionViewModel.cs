﻿using AVI_IT.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.ViewModels
{
    public class TransactionViewModel
    {
        public string customerName { get; set; }
        public Order order { get; set; }
        public CartModel cart { get; set; }

        public List<SelectListItem> customerList { get; set; }
       
    }
}
