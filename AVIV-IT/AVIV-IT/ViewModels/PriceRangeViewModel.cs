﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.ViewModels
{
    public class PriceRangeViewModel
    {
        public class PriceRange
        {
            public string range { get; set; }
            public bool bRange { get; set; }
        }
    }
}
