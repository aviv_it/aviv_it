﻿ using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.ViewModels
{
    public class AdminPageViewModel
    {

        public Dictionary<int, List<SelectListItem>> dict { get; set; }

        public Dictionary<int, string> selectedDict { get; set; }

        public Dictionary<int, List<SelectListItem>> dictQoute { get; set; }

        public Dictionary<int, string> selectedDictQuote { get; set; }

    }
}
