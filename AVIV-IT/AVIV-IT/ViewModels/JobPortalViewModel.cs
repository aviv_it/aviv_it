﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.ViewModels
{
    public class JobPortalViewModel
    {
        [Key]
        public int jobID { get; set; }
        [Display(Name = "Title")]
        public string jobTitle { get; set; }
        [Display(Name = "Category")]
        public string jobType { get; set; }
        [Display(Name = "Description")]
        public string jobDescription { get; set; }
        public DateTime jobPublishedDate { get; set; }

        // full address or could be just the state or country
        public string jobLocation { get; set; }
        public string empID { get; set; }

        [NotMapped]
        public List<SelectListItem> typeList { get; set; }

        [NotMapped]
        public List<SelectListItem> dateList { get; set; }

        public JobPortalViewModel()
        {
            typeList = new List<SelectListItem>();
            dateList = new List<SelectListItem>();
        }

    }
}
