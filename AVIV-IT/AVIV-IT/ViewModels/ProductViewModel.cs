﻿using AVI_IT.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using static AVI_IT.ViewModels.PriceRangeViewModel;

namespace AVI_IT.ViewModels
{
    

    public class ProductViewModel
    {
        [Key]
        public int productID { get; set; }
        public string productName { get; set; }
        public int typeID { get; set; }
        public string productDes { get; set; }
        public int stock { get; set; }
        public double price { get; set; }

        [NotMapped]
        public string fullDescription { get; set; }

        //public ProductType[] proType { get; set; }
        [NotMapped]
        public PriceRange proRange { get; set; }
        [NotMapped]
        public PriceRange[] proRangeArray { get; set; }
        [NotMapped]
        public CartModel cart { get; set; }
        [NotMapped]
        public LoginViewModel login { get; set; }
        [NotMapped]
        public ProductType productType { get; set; }
        [NotMapped]
        public List<ProductType> ptList { get; set; }
        [NotMapped]
        public List<ProductViewModel> prodList { get; set; }
    }
}
