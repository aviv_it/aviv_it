﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.Models
{
    public class Service
    {

        public int serviceID { get; set; }
        [Required]
        [Display(Name ="Title")]
        public string serviceTitle { get; set; }
        [Required]
        [Display(Name = "Content")]
        public string serviceContent { get; set; }

        public Service()
        {

        }
        public Service(int id, string title, string content)
        {
            this.serviceID = id;
            this.serviceTitle = title;
            this.serviceContent = content;
        }
    }
}
