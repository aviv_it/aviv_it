﻿using AVI_IT.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.Models
{
    public class CommentModel
    {

        [Key]
        public int commentID { get; set; }
        [Display(Name ="Comment")]
        [MinLength(5, ErrorMessage = "Comment is too short")]
        public string comment { get; set; }
        public string reply { get; set; }
        public int blogID { get; set; }
        public string userID { get; set; }
        [NotMapped]
        public IdentityUserHelper user { get; set; }
    }
}
