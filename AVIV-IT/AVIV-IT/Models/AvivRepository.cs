﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace AVI_IT.Models
{
    public class AvivRepository : IAvivRepository
    {
        private readonly AvivDataContext _db;
        List<Quotation> _qList;
        public AvivRepository(AvivDataContext db)
        {
            _db = db;
        }
        public IEnumerable<Quotation> GetAllQuotations()
        {
            _qList = _db.quotation.ToList<Quotation>();
            return _qList;
        }

        public Quotation Add(Quotation q)
        {
            _db.quotation.Add(q);
            return q;
        }
    }
}
