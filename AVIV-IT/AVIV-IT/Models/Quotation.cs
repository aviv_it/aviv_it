﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.Models
{
    public class Quotation
    {
        [Key]
        [Display(Name = "QuoteID")]
        public int quoteID { get; set; }
        [Display(Name = "First Name")]
        [Required(ErrorMessage = "First Name is required")]
        //[MinLength(2, ErrorMessage = "First name is too short")]
        [RegularExpression("^[A-Za-z,.'-]{2,}", ErrorMessage ="First name should be at least 2 letters long, with no special characters")]
        public string firstName { get; set; }
        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Last Name is required")]
        [RegularExpression("^[A-Za-z,.'-]{2,}", ErrorMessage = "Last name should be at least 2 letters long, with no special characters")]
        public string lastName { get; set; }
        [Display(Name = "Email")]
        [Required(ErrorMessage = "The email address is required")]
        //[EmailAddress(ErrorMessage = "The email address is not valid")]
        [RegularExpression(@"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z",
                            ErrorMessage = "Please enter a valid email address")]
        public string email { get; set; }
        [Display(Name = "Phone Number")]
        [Required(ErrorMessage = "Phone Number is required")]
        [Phone]
        [MinLength(10, ErrorMessage = "Phone number must be 10 digits")]
        [MaxLength(10,ErrorMessage = "Phone number must be 10 digits")]
        public string phone { get; set; }
        [Display(Name = "Description")]
        [MinLength(20, ErrorMessage = "Description is too short")]
        [Required(ErrorMessage = "Description is required")]
        public string description { get; set; }
        public int? empID { get; set; }
        public string userID { get; set; }
        [NotMapped]
        [Display(Name = "Assigned Employee")]
        public string empName { get; set; }
    }
}
