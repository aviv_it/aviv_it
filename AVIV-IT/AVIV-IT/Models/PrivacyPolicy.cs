﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.Models
{
    public class PrivacyPolicy
    {
        [Key]
        [Display(Name = "Heading ID")]

        public int headingId { get; set; }
        [Display(Name = "Heading Name")]
        public string headingName { get; set; }
        [Display(Name = "Text")]
        public string description { get; set; }
        [NotMapped]    
        public string document { get; set; }
    }
}
