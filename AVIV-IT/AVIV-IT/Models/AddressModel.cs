﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.Models
{
    public class AddressModel
    {
        public string addressID { get; set; }
        [Display(Name = "AddressLine1")]
        public string addressLine1 { get; set; }
        [Display(Name = "Suburb")]
        [Required]
        [MinLength(3, ErrorMessage = "Suburb must be more than 3 characters")]
        [RegularExpression(@"^(([A-za-z]+[\s]{1}[A-za-z]+)|([A-Za-z]+))$", ErrorMessage = "Enter a valid suburb")]
        public string suburb { get; set; }
        [Display(Name = "Postcode")]
        [Required]
        /*[MaxLength(4, ErrorMessage = "Postcode must be m 4 digits ")]
        [MinLength(4, ErrorMessage = "Postcode must be 4 digits ")]*/
        //[RegularExpression(@"^(([A-za-z]+[\s]{1}[A-za-z]+)|([A-Za-z]+))$", ErrorMessage = "Enter a valid postcode")]
        public int? postcode { get; set; }
        [Display(Name = "State")]
        [Required]
        [MinLength(3, ErrorMessage = "State must be more than 3 characters")]
        [RegularExpression(@"/^[0-9]{4}$/", ErrorMessage = "Enter a valid state")]
        public string state { get; set; }
        [Display(Name = "Country")]
        [Required]
        [MinLength(2, ErrorMessage = "Country must be more than 2 characters")]
        [RegularExpression(@"^(([A-za-z]+[\s]{1}[A-za-z]+)|([A-Za-z]+))$", ErrorMessage = "Enter a valid country name")]
        public string country { get; set; }

    }
}
