﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.Models
{
    public class Keys
    {
        public string publicKey { get; set; }
        public int amount { get; set; }
    }
}
