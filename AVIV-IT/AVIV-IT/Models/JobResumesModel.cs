﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.Models
{
    public class JobResumesModel
    {
        [Key]
        public int resumeID { get; set; }
        public string resumecontent { get; set; }
        public string coverlettercontent { get; set; }
        public int jobID { get; set; }

    }
}
