﻿
using AVI_IT.ViewModels;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.Models
{
    public class AvivDataContext : IdentityDbContext<IdentityUserHelper>
    {
        
        public AvivDataContext()
        {

        }
        public DbSet<Quotation> quotation { get; set; }
        public DbSet<HelpDeskViewModel> helpdesk { get; set; }
        public DbSet<Employee> employee { get; set; }
        public DbSet<BlogViewModel> blog { get; set; }
        public DbSet<LeaveCommentViewModel> comment { get; set; }
        public DbSet<PrivacyPolicy> policy { get; set; }
        //public DbSet<ProductViewModel> product { get; set; }
        public DbSet<JobPortalViewModel> job { get; set; }
        public DbSet<JobResumesModel> jobresumes { get; set; }
        public DbSet<ProductType> product_Type { get; set; }
        public DbSet<Product> product { get; set; }

        public DbSet<Service> service { get; set; }

        public DbSet<CartModel> cart { get; set; }
        public DbSet<ProductCart> product_cart { get; set; }
        public DbSet<Order> orders { get; set; }
       
        public AvivDataContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
    }
}
    

    

