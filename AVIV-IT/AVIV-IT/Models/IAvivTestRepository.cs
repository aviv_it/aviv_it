﻿using AVI_IT.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.Models
{
    public interface IAvivTestRepository
    {
        IEnumerable<Quotation> GetAllQuotations();
        Quotation Add(Quotation q);

        int GetCount();

    }
}
