﻿using AVI_IT.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.Models
{
    public class Order
    {
        [Key]
        public int ID { get; set; }
        [ForeignKey("productCartID")]
        public int productCartID { get; set; }
        public string selectedVariant { get; set; }
        public double totalCartPrice { get; set; }

        public double grandTotal { get; set; }
        //[ForeignKey("productCartID")]
        [NotMapped]
        public IdentityUserHelper user { get; set; }
        [NotMapped]
        public ShipingDetailViewModel shipping { get; set; }

        [NotMapped]
        public List<ProductCart> cartList { get; set; }
        [NotMapped]
        public CartModel cartUser { get; set; }
        [NotMapped]
        public List<Product> productList { get; set; }
        [NotMapped]

        public List<string> shippingVariant { get; set; }
        [NotMapped]
        public Keys keys { get; set; }
        [Display(Name = "AddressLine1")]
        [Required(ErrorMessage = "AddressLineRequired")]
        public string orderAddressLine1 { get; set; }
        [Display(Name = "Suburb")]
        [Required]
        [MinLength(3, ErrorMessage = "Suburb must be more than 3 characters")]
        [RegularExpression(@"^(([A-za-z]+[\s]{1}[A-za-z]+)|([A-Za-z]+))$", ErrorMessage = "Enter a valid suburb")]
        public string orderSuburb { get; set; }
        [Display(Name = "Postcode")]
        [Required]
        /*[MaxLength(4, ErrorMessage = "Postcode must be m 4 digits ")]
        [MinLength(4, ErrorMessage = "Postcode must be 4 digits ")]*/
        //[RegularExpression(@"^(([A-za-z]+[\s]{1}[A-za-z]+)|([A-Za-z]+))$", ErrorMessage = "Enter a valid postcode")]
        public int? orderPostcode { get; set; }
        [Display(Name = "State")]
        [Required]
        [MinLength(3, ErrorMessage = "State must be more than 3 characters")]
        [RegularExpression(@"/^[0-9]{4}$/", ErrorMessage = "Enter a valid state")]
        public string orderState { get; set; }
        [Display(Name = "Country")]
        [Required]
        [MinLength(2, ErrorMessage = "Country must be more than 2 characters")]
        [RegularExpression(@"^(([A-za-z]+[\s]{1}[A-za-z]+)|([A-Za-z]+))$", ErrorMessage = "Enter a valid country name")]
        public string orderCountry { get; set; }
        [NotMapped]
        public bool sameAddress { get; set; }
    }
}
