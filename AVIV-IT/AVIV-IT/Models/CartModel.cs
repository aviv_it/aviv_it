﻿using AVI_IT.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.Models
{
    public class CartModel
    {
        [Key]
        public int cartID { get; set; }
        
        public string userID { get; set; }

    }
}
