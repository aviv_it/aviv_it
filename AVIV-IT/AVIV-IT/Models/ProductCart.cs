﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.Models
{
    public class ProductCart
    {
        [Key]
        public int id { get; set; }
        public int cartID { get; set; }
        public int productID { get; set; }
        public int quantity { get; set; }
        public double totalCost { get; set; }
    }
}
