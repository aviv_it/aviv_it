﻿using AVI_IT.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.Models
{
    public class ProductType
    {

        // public string categoryID { get; set; }
        [Key]
        public int typeID { get; set; }
        [Display(Name = "Product type")]
        [Required(ErrorMessage = "Please enter Product type")]
        public string type { get; set; }
        [NotMapped]
        public bool bType { get; set; }

    }
}
