﻿using AVI_IT.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using static AVI_IT.ViewModels.PriceRangeViewModel;

namespace AVI_IT.Models
{
    public class Product
    {
        [Key]
        public int productID { get; set; }
        [Required]
        [Display(Name = "Product name")]
        public string productName { get; set; }
        [Required]
        [Display(Name = "Product descritpion")]
        [DataType(DataType.MultilineText)]
        public string productDes { get; set; }
        [Required]
        [Display(Name = "Number in stock")]
        public int stock { get; set; }
        [Required]
        [Display(Name = "Price")]
        public double price { get; set; }
        [Display(Name = "Image")]
        public string image { get; set; }
        [Required]
        [Display(Name = "Product type")]

        public int typeID { get; set; }
        [ForeignKey("typeID")]
        public ProductType productType { get; set; }


        [NotMapped]
        public string fullDescription { get; set; }

        //public ProductType[] proType { get; set; }
        [NotMapped]
        public PriceRange proRange { get; set; }
        [NotMapped]
        public PriceRange[] proRangeArray { get; set; }
        [NotMapped]
        public ProductCart cart { get; set; }
        [NotMapped]
        public LoginViewModel login { get; set; }
        [NotMapped]
        public List<ProductType> ptList { get; set; }
        //[NotMapped]
        //public List<ProductViewModel> prodList { get; set; }
        [NotMapped]
        public int prodCount { get; set; }
    }
}
