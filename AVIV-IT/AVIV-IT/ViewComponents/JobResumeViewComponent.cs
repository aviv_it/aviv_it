﻿using AVI_IT.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.ViewComponents
{
    public class JobResumeViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            JobResumesModel resume = new JobResumesModel();
            return View("JobResume", resume);
        }
    }
}
