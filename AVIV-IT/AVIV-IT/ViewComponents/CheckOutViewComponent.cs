﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using AVI_IT.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using AVI_IT.ViewModels;

namespace AVI_IT.ViewComponents
{
    public class CheckOutViewComponent:ViewComponent
    {
        private readonly IConfiguration config;

        public CheckOutViewComponent(IConfiguration config)
        {
            this.config = config;
        }
        public IViewComponentResult Invoke(Order order)
        {
            string myValue = config["Stripe:PublishableKey"];
            Keys key = new Keys { publicKey = myValue };
            if(order.selectedVariant.Equals("Express Shipping $21"))
            {
                order.grandTotal = order.totalCartPrice + 21;
            }
            else
            {
                order.grandTotal = order.totalCartPrice;
            }
            
            key.amount = Convert.ToInt32(order.grandTotal) * 100;
            HttpContext.Session.SetString("grandTotal", key.amount.ToString());
            order.keys = key;

            return View("Pay", order);
        }
    }
}
