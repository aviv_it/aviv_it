﻿using AVI_IT.Models;
using AVI_IT.Utilities;
using AVI_IT.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.ViewComponents
{
    public class OnlineShoppingViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            List<Product> products = HttpContext.Session.Get<List<Product>>("products");
            if (products == null)
            {
                products = new List<Product>();
            }

            return View("Cart1", products);
        }
       
    }
}
