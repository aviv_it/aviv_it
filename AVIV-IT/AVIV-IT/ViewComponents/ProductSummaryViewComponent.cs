﻿using AVI_IT.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.ViewComponents
{
    public class ProductSummaryViewComponent:ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            ProductViewModel result = new ProductViewModel();

            return View("Item", result);
        }
    }
}
