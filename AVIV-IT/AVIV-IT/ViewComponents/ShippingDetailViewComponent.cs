﻿using AVI_IT.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.ViewComponents
{
    public class ShippingDetailViewComponent:ViewComponent
    {
        public IViewComponentResult Invoke(ShipingDetailViewModel shipping)
        {
            ShipingDetailViewModel result = new ShipingDetailViewModel();
            result = shipping;
            return View("Shipping", result);
        }
    }
}
