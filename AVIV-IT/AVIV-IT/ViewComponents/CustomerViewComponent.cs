﻿using AVI_IT.Models;
using AVI_IT.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.ViewComponents
{
    public class CustomerViewComponent:ViewComponent
    {
        private readonly AvivDataContext _db;
        private readonly UserManager<IdentityUserHelper> userManager;

        public CustomerViewComponent(AvivDataContext db, UserManager<IdentityUserHelper> userManager)
        {
            _db = db;
            this.userManager = userManager;
        }
        public IViewComponentResult Invoke()
        {
            TransactionViewModel trans = new TransactionViewModel()
            {
                customerName = ""
            };
            FillArray(trans);
            return View("CustomerList", trans);
        }

        private void FillArray(TransactionViewModel customer)
        {
            List<IdentityUserHelper> userL = new List<IdentityUserHelper>();
            //List<TransactionViewModel> allUser = new List<TransactionViewModel>();
            List<string> userIDList = new List<string>();
            var Result = from v in _db.cart
                         select v;
            var users = userManager.Users;

            customer.customerList = new List<SelectListItem>();
            foreach (var user in users)
            {
                userL.Add(user);
            }

            foreach (var u in userL)
            {
                foreach (var cus in Result)
                {
                    if (cus.userID == null)
                    { }
                    else
                    {
                        if (cus.userID == u.Id)
                        {
                            SelectListItem item = new SelectListItem();
                            if (!userIDList.Contains(u.Id))
                            {
                                item.Text = u.UserName;
                                item.Value = u.UserName;
                                userIDList.Add(u.Id);
                                customer.customerList.Add(item);
                            }

                        }
                    }


                }
            }

        }

    }
}
