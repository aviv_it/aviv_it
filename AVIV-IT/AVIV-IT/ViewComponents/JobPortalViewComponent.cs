﻿using AVI_IT.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.ViewComponents
{
    public class JobPortalViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            JobPortalViewModel jpvc = new JobPortalViewModel();

            return View("SearchBar", jpvc);
        }
    }
}
