﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AVI_IT.Models;
using AVI_IT.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace AVI_IT.Controllers
{
    public class JobPortalController : Controller
    {
        private readonly AvivDataContext _db;
        private readonly IWebHostEnvironment _he;

        public JobPortalController(AvivDataContext db, IWebHostEnvironment _he)
        {
            this._db = db;
            this._he = _he;
        }




        [HttpGet]
        public IActionResult Job()
        {
            return View();
        }

        [HttpGet]
        public IActionResult JobDescription(int? selectedJob)
        {
            /* var Result = from j in _db.job 
                          where j.jobID == selectedJob
                                   select new JobPortalViewModel
                                   {
                                       jobID = j.jobID,
                                       jobTitle = j.jobTitle,
                                    jobDescription = j.jobDescription,
                                     jobLocation = j.jobLocation
                                   };*/
            JobPortalViewModel job = _db.job.Find(selectedJob);
            //var p = from v in _db.job
            //        where v.jobID == selectedJob
            //        select v;

            //FillArrayCategory(jobPortal);
            return View(job);
        }

        [HttpGet]
        public IActionResult DisplayAll()
        {
            var Result = from j in _db.job
                         select new JobPortalViewModel
                         {
                             jobID = j.jobID,
                             jobTitle = j.jobTitle,
                             jobDescription = j.jobDescription,
                             jobLocation = j.jobLocation
                         };

            //for front end
            JobPortalViewModel jobPortal = new JobPortalViewModel
            {
                jobTitle = "",
                jobDescription = "",
                jobLocation = ""
            };

            FillArrayCategory(jobPortal);
            return View(Result);

        }

        public IActionResult JobAdmin()
        {
            var Result = from j in _db.job
                         select new JobPortalViewModel
                         {
                             jobID = j.jobID,
                             jobTitle = j.jobTitle,

                         };

            return View(Result);
        }
        [HttpGet]
        public IActionResult SearchJobs(string jobType, string jobTitle)
        {

            var result1 = _db.job.Where(x => x.jobTitle.StartsWith(jobTitle) || jobTitle == null).ToList();
            if (result1 == null)
            {
                return NoJobResults();
            }
            else
            {
                return View(result1);
            }

        }

        public IActionResult NoJobResults()
        {
            return View();
        }


        [HttpPost]
        public async Task<IActionResult> Upload(JobResumesModel upl, IFormFile pdf)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (pdf != null)
                    {
                        var name = Path.Combine(_he.WebRootPath + "/images", Path.GetFileName(pdf.FileName));
                        await pdf.CopyToAsync(new FileStream(name, FileMode.Create));
                        upl.resumecontent = "images/" + pdf.FileName;
                    }
                    if (pdf == null)
                    {
                        upl.resumecontent = "images/imgNotFound.PNG";
                    }
                    _db.jobresumes.Add(upl);
                    await _db.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);

            }
            return RedirectToAction(nameof(Upload));

        }




        private void FillArrayCategory(JobPortalViewModel list)
        {
            SelectListItem item = new SelectListItem();
            item.Text = "Full-Time";
            item.Value = "Full-Time";

            list.typeList.Add(item);

            item = new SelectListItem();
            item.Text = "Part-Time";
            item.Value = "Part-Time";

            list.typeList.Add(item);

            item = new SelectListItem();
            item.Text = "Casual";
            item.Value = "Casual";

            list.typeList.Add(item);

            item = new SelectListItem();
            item.Text = "Contrator";
            item.Value = "Contrator";

            list.typeList.Add(item);

            item = new SelectListItem();
            item.Text = "Newest";
            item.Value = "Newest";

            list.dateList.Add(item);

            item = new SelectListItem();
            item.Text = "Oldest";
            item.Value = "Oldest";

            list.dateList.Add(item);
        }


    }
}
