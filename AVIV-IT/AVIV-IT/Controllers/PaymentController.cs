﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Stripe;
using AVI_IT.Models;
using Microsoft.AspNetCore.Http;
using AVI_IT.Utilities;

namespace Stripe1.Controllers
{
    [Authorize]
    public class PaymentController : Controller
    {
        private readonly IConfiguration config;
        private readonly AvivDataContext _db;

        public PaymentController(IConfiguration config, AvivDataContext db)
        {
            this.config = config;
            _db = db;
        }
        public IActionResult Index()
        {
            //string myValue = config["Stripe:PublishableKey"];
            //Keys key = new Keys { publicKey = myValue };
            return View();
        }
        public IActionResult Charge(string stripeEmail, string stripeToken, AVI_IT.Models.Order order)
        {
            try
            {
                var amount = HttpContext.Session.GetString("grandTotal");
                int total = Convert.ToInt32(amount);

                var customers = new CustomerService();
                var charges = new ChargeService();

                var customer = customers.Create(new CustomerCreateOptions
                {
                    Email = stripeEmail,
                    Source = stripeToken
                });
                var charge = charges.Create(new ChargeCreateOptions
                {
                    Amount = total,
                    Description = "Amount to pay",
                    Currency = "AUD",
                    CustomerId = customer.Id
                });
                AVI_IT.Models.Order o = new AVI_IT.Models.Order();
                o = order;
                _db.orders.Add(o);
                _db.SaveChanges();
                HttpContext.Session.Remove("products");
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);

            }
            
            return View();
        }
        public IActionResult Error()
        {
            return View();
        }
    }
}
