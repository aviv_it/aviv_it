﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AVI_IT.Models;
using Microsoft.AspNetCore.Mvc;

namespace AVI_IT.Controllers
{
    public class PrivacyPolicyController : Controller
    {
        private readonly AvivDataContext _db;
        public PrivacyPolicyController(AvivDataContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            List<PrivacyPolicy> lPolicy = _db.policy.ToList();
            return View(lPolicy);
        }
        [HttpGet]
        public IActionResult Create()
        {
            try
            {
                return View();
            }
            catch(Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message); ;
            }
            return View();
        }
            
        [HttpPost]
        public IActionResult Create(PrivacyPolicy pPolicy)
        {
           try
            { 
            
                if (!ModelState.IsValid) return View(pPolicy);

                _db.policy.Add(pPolicy);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message); ;
            }
            return RedirectToAction("Index", pPolicy);
        }
        public IActionResult Display()
        {
            List<PrivacyPolicy> lPolicy = _db.policy.ToList();
            return View(lPolicy);
        }
    }
}
