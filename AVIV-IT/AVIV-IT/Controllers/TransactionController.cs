﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Syncfusion.Drawing;
using Syncfusion.Pdf;
using Syncfusion.HtmlConverter;
using Syncfusion.Pdf.Graphics;
using Syncfusion.Pdf.Grid;
using Wkhtmltopdf.NetCore;
using AVI_IT.Models;
using Microsoft.AspNetCore.Http;
using AVI_IT.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace AVI_IT.Controllers
{
    [Authorize]
    public class TransactionController : Controller
    {
        private readonly AvivDataContext _db;
        private readonly UserManager<IdentityUserHelper> userManager;

        public TransactionController(AvivDataContext db,UserManager<IdentityUserHelper> userManager)
        {
            _db = db;
            this.userManager = userManager;
        }

        [HttpGet]
        public IActionResult Index()
        {
            TransactionViewModel trans = new TransactionViewModel()
            {
                customerName = ""
            };
            FillArray(trans);
            return View(trans);
        }

        [HttpGet]
        public IActionResult Sale(List<Order> tranList)
        {
            List<Order> list = new List<Order>();

            foreach(var item in tranList)
            {
                list.Add(item);
            }

            return View(list);
        }


        public async Task<ActionResult> CreateDocument(TransactionViewModel trans)
        {
            List<Order> orderList = new List<Order>();
            List<Order> allOrder = new List<Order>();
            List<Order> nullOrder = new List<Order>();
            List<IdentityUserHelper> userList = new List<IdentityUserHelper>();
            List<string> userNameList = new List<string>();
            

            IEnumerable<Order> dataTable = null;
            //Create a new PDF document.
            PdfDocument doc = new PdfDocument();
            //Add a page.
            PdfPage page = doc.Pages.Add();
            //Create a PdfGrid.
            PdfGrid pdfGrid = new PdfGrid();
            var Result = from v in _db.orders
                         select new Order {
                             ID = v.ID,
                             productCartID = v.productCartID,
                             selectedVariant = v.selectedVariant,
                             grandTotal = v.grandTotal,
                             totalCartPrice = v.totalCartPrice,
                             orderAddressLine1 = v.orderAddressLine1,
                             orderSuburb = v.orderSuburb,
                             orderState = v.orderState,
                             orderPostcode = v.orderPostcode,
                             orderCountry = v.orderCountry,
                             //cartUser = v.cartUser
                         };

            var cartResult = from c in _db.cart
                             select new CartModel
                             {
                                 cartID = c.cartID,
                                 userID = c.userID
                             };
            
            var users = userManager.Users;

            foreach(var user in users)
            {
                userList.Add(user);
                
            }

            List<Order> p = new List<Order>();
            

            foreach (var user in userList)
             {
                foreach (var cart in cartResult)
                {
                    if (cart.userID == user.Id)
                    {
                        if (!userNameList.Contains(user.UserName))
                        {
                            userNameList.Add(user.UserName);
                        }
                        
                    }

                }
                
             }
            
            if (ModelState.IsValid)
            {
                var newResult = from v in _db.orders
                                join c in _db.cart
                                on v.productCartID equals c.cartID
                                select new
                                {
                                    ID1 = v.ID,
                                    productCartID1 = v.productCartID,
                                    selectedVariant1 = v.selectedVariant,
                                    grandTotal1 = v.grandTotal,
                                    totalCartPrice1 = v.totalCartPrice,
                                    orderAddressLine = v.orderAddressLine1,
                                    orderSuburb1 = v.orderSuburb,
                                    orderState1 = v.orderState,
                                    orderPostcode1 = v.orderPostcode,
                                    orderCountry1 = v.orderCountry,
                                    cartID1 = c.cartID,
                                    userID1 = c.userID

                                };

                if(newResult != null)
                {
                    foreach (var h in newResult)
                    {

                        var us = await userManager.FindByIdAsync(trans.customerName);

                        if (us.Id == h.userID1)
                        {
                           
                                orderList.Add(new Order
                                {
                                    ID = h.ID1,
                                    productCartID = h.productCartID1,
                                    selectedVariant = h.selectedVariant1,
                                    grandTotal = h.grandTotal1,
                                    totalCartPrice = h.totalCartPrice1,
                                    orderAddressLine1 = h.orderAddressLine,
                                    orderSuburb = h.orderSuburb1,
                                    orderState = h.orderState1,
                                    orderPostcode = h.orderPostcode1,
                                    orderCountry = h.orderCountry1,

                                });
                          
                        }
                        else
                        {
                            nullOrder = null;
                            dataTable = nullOrder;
                            pdfGrid.DataSource = dataTable;
                        }

                    }


                    dataTable = orderList;
                    pdfGrid.DataSource = dataTable;


                }
                else
                {
                    if (Result != null)
                    {
                        foreach (var item in Result)
                        {
                            allOrder.Add(new Order
                            {
                                ID = item.ID,
                                productCartID = item.productCartID,
                                selectedVariant = item.selectedVariant,
                                grandTotal = item.grandTotal,
                                totalCartPrice = item.totalCartPrice,
                                orderAddressLine1 = item.orderAddressLine1,
                                orderSuburb = item.orderSuburb,
                                orderState = item.orderState,
                                orderPostcode = item.orderPostcode,
                                orderCountry = item.orderCountry

                            });


                        }
                        dataTable = allOrder;
                        pdfGrid.DataSource = dataTable;
                    }

                }

                //Draw grid to the page of PDF document.
                pdfGrid.Draw(page, new Syncfusion.Drawing.PointF(30, 30));
                //Save the PDF document to stream
                MemoryStream stream = new MemoryStream();
                doc.Save(stream);
                //If the position is not set to '0' then the PDF will be empty.
                stream.Position = 0;
                //Close the document. 
                doc.Close(true);
                //Defining the ContentType for pdf file.
                string contentType = "application/pdf";
                //Define the file name.
                string fileName = "Transaction.pdf";
                //Creates a FileContentResult object by using the file contents, content type, and file name.
                return File(stream, contentType, fileName);
            } else return RedirectToAction("Index");
            }
           
       

        private void  FillArray(TransactionViewModel customer)
        {
            List<IdentityUserHelper> userL = new List<IdentityUserHelper>();
            //List<TransactionViewModel> allUser = new List<TransactionViewModel>();
            List<string> userIDList = new List<string>();
            var Result = from v in _db.cart
                         select v;
            var users =  userManager.Users;
            
             customer.customerList =  new List<SelectListItem>();
            foreach (var user in users)
            {
                userL.Add(user);
            }

            foreach (var u in userL)
            {
                 foreach(var cus in Result)
                  {
                        if (cus.userID == null)
                        { }
                        else
                        {
                            if (cus.userID == u.Id)
                            {
                                SelectListItem item = new SelectListItem();
                                if(!userIDList.Contains(u.Id))
                                {
                                    item.Text = u.UserName;
                                    item.Value = u.Id;
                                    userIDList.Add(u.Id);
                                    customer.customerList.Add(item);
                                }
                            
                            }
                        }
                    
                        
                    }
            }

        }
    }
}

