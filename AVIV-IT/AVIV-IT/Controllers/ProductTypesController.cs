﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AVI_IT.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AVI_IT.Controllers
{
    [Authorize(Roles = "Admin, " +
                       "Employee")]
    public class ProductTypesController : Controller
    {
        private readonly AvivDataContext _db;

        public ProductTypesController(AvivDataContext db)
        {
            this._db = db;
        }
        public IActionResult Index()
        {
            var data = _db.product_Type;
            return View(data.ToList());
        }
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(ProductType productTypes)
        {
            if(ModelState.IsValid)
            {
                _db.product_Type.Add(productTypes);
                await _db.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(productTypes);
        }
        [HttpGet]
        public IActionResult Edit(int? id)
        {
            if(id==null)
            {
                return NotFound();
            }

            var productType = _db.product_Type.Find(id);
            if(productType == null)
            {
                return NotFound();
            }
            return View(productType);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(ProductType productTypes)
        {
            if (ModelState.IsValid)
            {
                _db.Update(productTypes);
                await _db.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(productTypes);
        }
        [HttpGet]
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productType = _db.product_Type.Find(id);
            if (productType == null)
            {
                return NotFound();
            }
            return View(productType);
        }
        [HttpPost]
        public IActionResult Details(ProductType productTypes)
        {

                return RedirectToAction(nameof(Index));
        }
        [HttpGet]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productType = _db.product_Type.Find(id);
            if (productType == null)
            {
                return NotFound();
            }
            return View(productType);
        }
        [HttpPost]
        public async Task<IActionResult> Delete(int? id,ProductType productTypes)
        {
            if(id == null)
            {
                return NotFound();
            }
            if(id != productTypes.typeID)
            {
                return NotFound();
            }
            var productType = _db.product_Type.Find(id);
            if (productType == null)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                _db.Remove(productType);
                await _db.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(productTypes);
        }
    }
}
