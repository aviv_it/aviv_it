﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AVI_IT.Models;
using AVI_IT.Utilities;
using AVI_IT.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using static AVI_IT.ViewModels.PriceRangeViewModel;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using System;

namespace AVI_IT.Controllers
{
  //  [Authorize]
    public class OnlineShoppingController : Controller
    {
        private readonly AvivDataContext _db;
        private UserManager<IdentityUserHelper> userManager;
        public readonly IConfigurationRoot configuration;
        public AddressModel address = new AddressModel();
        public OnlineShoppingController(AvivDataContext db, IWebHostEnvironment env, UserManager<IdentityUserHelper> userManager)
        {
            _db = db;
            this.userManager = userManager;
            configuration = new ConfigurationBuilder()
                       .AddEnvironmentVariables()
                       .AddJsonFile(env.ContentRootPath + "/config.json")
                       .Build();
        }
        public IActionResult Index()
        {
            try
            {
                List<Product> lProd = getList();
                return View(lProd);
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);

            }
            return NotFound();
        }

        //Get product details
        public IActionResult Details(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                List<Product> lp = getList();
                foreach (var item in lp)
                {
                    if (item.productID == id)
                    {
                        return View(item);
                    }
                }
                
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);

            }
            return NotFound();
        }

        //Post product details
        [HttpPost]
        [ActionName("Details")]
        public IActionResult ProductDetails(int? id)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    List<Product> products = new List<Product>();
                    if (id == null)
                    {
                        return NotFound();
                    }
                    addProduct(id);
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);

            }
            return RedirectToAction(nameof(Index));

        }
        [HttpGet]
        [ActionName("Remove")]
        public IActionResult RemoveFromCart(int? id)
        {
            try
            {
                List<Product> products = HttpContext.Session.Get<List<Product>>("products");
                if (products != null)
                {
                    var product = products.FirstOrDefault(m => m.productID == id);
                    if (product != null)
                    {
                        products.Remove(product);
                        HttpContext.Session.Set("products", products);

                    }
                    return View("Cart", products);
                }
                else
                {
                    return View("EmptyCart");
                }
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);

            }
            return NotFound();

        }
        public IActionResult Cart(int? id)
        { 
            try
            {
                List<Product> products = HttpContext.Session.Get<List<Product>>("products");
                if (products != null)
                {
                    if (products.Count() > 0)
                    {
                        HttpContext.Session.Set("products", products);

                        return View(products);
                    }
                    else
                        return View("EmptyCart");

                }
                else
                {
                    return View("EmptyCart");
                }
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);

            }
            return NotFound();
        }
        public IActionResult EmptyCart()
        {
            return View();
        }
        
        [HttpGet]
        [ActionName("Add")]
        public IActionResult AddToCart(int? id)
        {
          try
            {
                List<Product> products = addProduct(id);
                return View("Cart", products);
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);

            }
            return NotFound();
            
        }
        [HttpPost]
        public IActionResult Add(int? id)
        {
            try
            {
                List<Product> products = addProduct(id);
                return View("Cart", products);
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);

            }
            return NotFound();
            
        }
        [HttpGet]
        [ActionName("RemoveOne")]
        public IActionResult RemoveOneFromCart(int? id)
        {
            try
            {
                List<Product> products = removeProduct(id);
                Product prod = new Product();
                prod = products.FirstOrDefault(m => m.productID == id);
                if (prod != null)
                {
                    if (prod.prodCount == 0)
                    {
                        products.Remove(prod);
                        HttpContext.Session.Set("products", products);
                        return View("Cart", products);
                    }
                }

                return View("Cart", products);
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);

            }
            return NotFound();
            
        }
        //[HttpPost]
        //public IActionResult RemoveOne(int? id)
        //{
        //    //List<Product> products = removeProduct(id);
        //    //return View("Cart", products);
        //    List<Product> products = removeProduct(id);
        //    Product prod = new Product();
        //    prod = products.FirstOrDefault(m => m.productID == id);
        //    if (prod != null)
        //    {
        //        if (prod.prodCount == 0)
        //        {
        //            products.Remove(prod);
        //            return View("Cart", products);
        //        }
        //    }

        //    return View("Cart", products);
        //}
        [Authorize]
        public async Task<IActionResult> Checkout()
        {
            try
            {
                List<Product> products = HttpContext.Session.Get<List<Product>>("products");
                CartModel cartUser = new CartModel();
                var userId = HttpContext.Session.GetString("userID");
                cartUser.userID = userId;
                _db.cart.Add(cartUser);
                _db.SaveChanges();
                HttpContext.Session.Set("cartUser", cartUser);

                foreach (var item in products)
                {
                    ProductCart cart = new ProductCart();
                    cart.cartID = cartUser.cartID;
                    cart.productID = item.productID;
                    cart.quantity = item.prodCount;
                    cart.totalCost = item.price * item.prodCount;

                    _db.product_cart.Add(cart);
                    _db.SaveChanges();
                }
               
                ShipingDetailViewModel shipping = new ShipingDetailViewModel();
                shipping = await getShipping(cartUser);
                //shipping.sameAd = false;
                return ViewComponent("ShippingDetail", shipping);
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);

            }
            return NotFound();
            
        }
        [HttpPost]
        public async Task<IActionResult> Checkout(ShipingDetailViewModel shipping)
        {
            try
            {
                 
                CartModel cartUser = new CartModel();
                cartUser = HttpContext.Session.Get<CartModel>("cartUser");
                
                //need to add shipping cost to totalCartPrice and sent to session
                ShipingDetailViewModel shippingForOrder = new ShipingDetailViewModel();
                shippingForOrder = await getShipping(cartUser);
               
                Order o = new Order();
                o.cartList = shippingForOrder.cartList;
                o.cartUser = shippingForOrder.cartUser;
                o.grandTotal = shippingForOrder.grandTotal;
                o.keys = shippingForOrder.keys;
                o.productCartID = shippingForOrder.cartUser.cartID;
                o.productList = shippingForOrder.productList;
                o.totalCartPrice = shippingForOrder.totalCartPrice;
                o.user = shippingForOrder.user;
                o.selectedVariant = shipping.selectedVariant;

                if(shipping.sameAd == false)
                {
                    o.orderAddressLine1 = shippingForOrder.user.addressLine1;
                    o.orderState = shippingForOrder.user.state;
                    o.orderPostcode = shippingForOrder.user.postcode;
                    o.orderSuburb = shippingForOrder.user.suburb;
                    o.orderCountry = shippingForOrder.user.country;
                }
                else
                {
                    o.orderAddressLine1 = shipping.address.addressLine1;
                    o.orderState = shipping.address.state;
                    o.orderPostcode = shipping.address.postcode;
                    o.orderSuburb = shipping.address.suburb;
                    o.orderCountry = shipping.address.country;
                }

                return ViewComponent("CheckOut", o);
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);

            }
            return NotFound();
            
        }


        public async Task<ShipingDetailViewModel> getShipping (CartModel cartUser)
        {
            try
            {
                ShipingDetailViewModel shipping = new ShipingDetailViewModel();
                List<ProductCart> cList = new List<ProductCart>();
                List<Product> products = new List<Product>();
                IdentityUserHelper user = new IdentityUserHelper();
                //AddressModel address = new AddressModel();
                List<string> shippingVariant = new List<string> {

                    "Express Shipping $21",
                    "Free Shipping"
                    };
                

                shipping.shippingVariant = shippingVariant;
                shipping.selectedVariant = "";
                double totalCartAmmount = 0.0;
                products = HttpContext.Session.Get<List<Product>>("products");
                var userId = HttpContext.Session.GetString("userID");
                user = await userManager.FindByIdAsync(userId);
                //address
                address.addressLine1 = user.addressLine1;
                address.state = user.state;
                address.postcode = user.postcode;
                address.suburb = user.suburb;
                address.country = user.country;

                shipping.user = user;
                shipping.cartUser = cartUser;

                shipping.address = address;

                foreach (var item in _db.product_cart)
                {
                    if (item.cartID == cartUser.cartID)
                    {
                        cList.Add(item);
                    }
                }
                shipping.cartList = cList;
                foreach (var cost in cList)
                {
                    totalCartAmmount += cost.totalCost;
                }
                shipping.productList = products;
                shipping.totalCartPrice = totalCartAmmount;
                return shipping;
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);
                
            }
            return null;
        }

        public List<Product> getList()
        {
            try
            {
                PriceRange[] proRangeArray = {new PriceRange { range="0-$50", bRange=false},
            new PriceRange { range="$50-$100", bRange=false},
            new PriceRange { range="More than $100", bRange=false}};


                List<Product> products = _db.product.ToList();
                List<ProductType> pTypes = _db.product_Type.ToList();
                var listP = new List<Product>();

                var prodWithTypes = from p in products
                                    join t in pTypes on p.typeID equals t.typeID
                                    select new ProdTypeViewModel
                                    {
                                        product = p,
                                        pType = t
                                    };
                foreach (var item in prodWithTypes)
                {
                    Product p = new Product();
                    p.productID = item.product.productID;
                    p.productName = item.product.productName;
                    p.typeID = item.product.typeID;
                    p.productDes = item.product.productDes;
                    p.stock = item.product.stock;
                    p.price = item.product.price;
                    p.image = item.product.image;
                    if (p.price <= 50)
                    {
                        p.proRange = new PriceRange { range = "0-$50", bRange = false };
                    }
                    else if (p.price > 50 && p.price <= 100)
                    {
                        p.proRange = new PriceRange { range = "50-$100", bRange = false };
                    }
                    else
                    {
                        p.proRange = new PriceRange { range = "More than $100", bRange = false };
                    }
                    p.productType = item.pType;
                    p.ptList = pTypes;
                    p.proRangeArray = proRangeArray;

                    listP.Add(p);
                }
                return listP;

            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);

            }
            return null;
            
        }
        public List<Product> addProduct(int? id)
        {
            try
            {
                List<Product> products = new List<Product>();
                products = HttpContext.Session.Get<List<Product>>("products");
                if (products == null)
                {
                    products = new List<Product>();
                }
                List<Product> lp = getList();
                foreach (var item in lp)
                {
                    if (item.productID == id)
                    {
                        if (products.Count() > 0)
                        {
                            Product findProduct = products.ToList().FirstOrDefault(m => m.productID == id);
                            if (findProduct != null)
                            {
                                findProduct.prodCount++;
                            }
                            else
                            {
                                item.prodCount++;
                                products.Add(item);
                            }
                        }
                        else
                        {
                            products.Add(item);
                            foreach (var prod in products)
                            {
                                if (prod.productID == id)
                                {
                                    if (prod.prodCount == 0)
                                    {
                                        prod.prodCount++;
                                    }
                                }
                            }
                        }
                        HttpContext.Session.Set("products", products);
                    }
                }
                return products;
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);

            }
            return null; ;
            
        }
        public List<Product> removeProduct(int? id)
        {
            try
            {
                List<Product> products = new List<Product>();
                products = HttpContext.Session.Get<List<Product>>("products");
                if (products == null)
                {
                    products = new List<Product>();
                }
                List<Product> lp = getList();
                foreach (var item in lp)
                {
                    if (item.productID == id)
                    {
                        if (products.Count() > 0)
                        {
                            Product findProduct = products.ToList().FirstOrDefault(m => m.productID == id);
                            if (findProduct != null)
                            {
                                if (findProduct.prodCount > 0)
                                {
                                    findProduct.prodCount--;
                                }
                            }
                        }
                        HttpContext.Session.Set("products", products);
                    }
                }
                return products;
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);

            }
            return null;
            
        }
        
    }
}
