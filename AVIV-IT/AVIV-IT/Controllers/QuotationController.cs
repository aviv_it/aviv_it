﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AVI_IT.Models;
using AVI_IT.Services;
using AVI_IT.ViewModels;
using DNTCaptcha.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace AVI_IT.Controllers
{
    [Authorize]
    public class QuotationController : Controller
    {
        
        private readonly AvivDataContext _db;
        public readonly IConfigurationRoot configuration;
        public readonly IConfiguration con;
        private readonly IDNTCaptchaValidatorService _validatorService;
        private readonly DNTCaptchaOptions _catpchaOptions;
        public QuotationController (AvivDataContext db, IWebHostEnvironment env,
           IDNTCaptchaValidatorService validatorService, IOptions<DNTCaptchaOptions> catpchaOptions, IConfiguration con)
        {
            _db = db;
            configuration = new ConfigurationBuilder()
                       .AddEnvironmentVariables()
                       .AddJsonFile(env.ContentRootPath + "/config.json")
                       .Build();
            this._validatorService = validatorService;
            this._catpchaOptions = catpchaOptions == null ? throw new ArgumentNullException(nameof(catpchaOptions)) :
                                                                                          catpchaOptions.Value;
            this.con = con;
        }
        [HttpGet]
        public IActionResult Index()
        {
            Quotation q = new Quotation();
            try
            {
                //session variable passing userID
                q.userID = HttpContext.Session.GetString("userID");

                q.empID = null;//quotation was not assign to employee yet
            }
            catch(Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);
                return View();
            }
           
            return View(q);

        }
        [HttpPost]
        public IActionResult Index(Quotation q)
        {
            try
            {
                if (!ModelState.IsValid) return View(q);
                else
                {
                    if (!_validatorService.HasRequestValidCaptchaEntry(Language.English, DisplayMode.ShowDigits))
                    {
                        this.ModelState.AddModelError(_catpchaOptions.CaptchaComponent.CaptchaInputName, "Enter a valid key");
                        return View(q);
                    }
                    _db.quotation.Add(q);
                    _db.SaveChanges();
                }
                

                //sending email to the client after quotation is sent
                Email e = new Email();
                e.toEmail = q.email; 
                e.fromEmail = con.GetSection("Email").GetSection("FromEmail").Value;
                e.title = "Quotation request no " + q.quoteID;
                e.message = "Quotation request no " + q.quoteID + " has been submitted and it is under review. We will contact you shortly.";
                sendEmail(e);
                //sending email to AVIV IT after quotation is requested
                Email e1 = new Email();
                e1.toEmail = con.GetSection("Email").GetSection("FromEmail").Value;
                e1.fromEmail = con.GetSection("Email").GetSection("FromEmail").Value;
                e1.title = "Quotation request no " + q.quoteID + " from " + q.firstName + " " + q.lastName;
                e1.message = "Quotation request no " + q.quoteID + " from " + q.firstName + " " + q.lastName
                            + "\nEmail: " + q.email
                            + "\nPhone: " + q.phone
                            + "\nDescription: " + q.description;
                sendEmail(e1);
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);
                return View(q);
            }
            return View("Feedback");
        }
        //after clicking Done button this should redirect to the home page
        public IActionResult Feedback()
        {
            return RedirectToAction("Home", "Home");
        }
        [HttpPost]
        public IActionResult Display()
        {
            
                //linq to join employee,user and quotation tables
                List<Employee> employees = _db.employee.ToList();
            List<IdentityUserHelper> users = _db.Users.ToList();
            List<Quotation> quotations = _db.quotation.ToList();
            var quotationList = new List<Quotation>();
            try
            {
                var empUser = from e in employees
                              join u in users on e.userID equals u.Id into empUserTable
                              from u in empUserTable.ToList()
                              join q in quotations on e.employeeID equals q.empID into empUserTable2
                              from q in empUserTable2.ToList()
                              select new EmpUserViewModel
                              {
                                  employee = e,
                                  user = u,
                                  quotation = q
                              };

                foreach (var user in empUser)
                {
                    Quotation q = new Quotation();
                    q.quoteID = user.quotation.quoteID;
                    q.description = user.quotation.description;
                    q.firstName = user.quotation.firstName;
                    q.lastName = user.quotation.lastName;
                    q.phone = user.quotation.phone;
                    q.email = user.quotation.email;
                    q.empID = user.quotation.empID;
                    q.empName = user.user.firstName.ToString() + " " + user.user.lastName.ToString();

                    quotationList.Add(q);
                }

                return View(quotationList.OrderBy(model => model.quoteID));
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);
            }
            return View(quotationList.OrderBy(model => model.quoteID));
        }
        public void sendEmail(Email e)
        {
            string strKey = configuration.GetValue<string>("SendGridKey");
            EmailOp emailOp = new EmailOp(e.toEmail, e.fromEmail, e.title, e.message, strKey);
        }
    }
}
