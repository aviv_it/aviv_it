﻿using AVI_IT.Models;
using AVI_IT.Services;
using AVI_IT.ViewModels;
using DNTCaptcha.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AVI_IT.Controllers
{

    public class AccountController : Controller
    {
        private UserManager<IdentityUserHelper> userManager;
        private SignInManager<IdentityUserHelper> signInManager;
        private readonly IDNTCaptchaValidatorService _validatorService;
        private readonly DNTCaptchaOptions _catpchaOptions;
        public readonly IConfigurationRoot configuration;
        public readonly IConfiguration con;

        public AccountController(UserManager<IdentityUserHelper> userManager, SignInManager<IdentityUserHelper> signInManager,
           IDNTCaptchaValidatorService validatorService, IOptions<DNTCaptchaOptions> catpchaOptions, IWebHostEnvironment env, IConfiguration con)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this._validatorService = validatorService;
            this._catpchaOptions = catpchaOptions == null ? throw new ArgumentNullException(nameof(catpchaOptions)) :
                                                                                          catpchaOptions.Value;
            configuration = new ConfigurationBuilder()
                       .AddEnvironmentVariables()
                       .AddJsonFile(env.ContentRootPath + "/config.json")
                       .Build();
            this.con = con;
        }

        [HttpGet]
        public IActionResult Register()
        {

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel m)
        {
            try
            {
              

                if (ModelState.IsValid)
                {
                    if (!_validatorService.HasRequestValidCaptchaEntry(Language.English, DisplayMode.ShowDigits))
                    {
                        this.ModelState.AddModelError(_catpchaOptions.CaptchaComponent.CaptchaInputName, "Enter a valid key");
                        return View(m);
                    }
                    var user = new IdentityUserHelper
                    {

                        UserName = m.email,
                        Email = m.email,
                        firstName = m.firstName,
                        lastName = m.lastName,
                        PhoneNumber = m.phone,
                        addressLine1 = m.addressLine1,
                        suburb = m.suburb,
                        postcode = (int) m.postcode,
                        state = m.state,
                        country = m.country
                    };
                    if (m.IsChecked == true)
                    {
                        var result = await userManager.CreateAsync(user, m.password);
                        result = await userManager.AddToRoleAsync(user, "User");
                        if (result.Succeeded)
                        {
                            await signInManager.SignInAsync(user, isPersistent: false);
                            return RedirectToAction("Home", "Home");
                        }
                        foreach (var e in result.Errors)
                        {
                            ModelState.AddModelError("", e.Description);
                        }
                    }
                    ModelState.AddModelError("isChecked", "You must read and accept Privacy Policy");
                }
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);
                return View();
            }

            return View(m);
        }
        /*[HttpGet]
        public IActionResult Feedback()
        {
            return RedirectToAction("Login");
        }*/

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel m)
        {
            try
            {

                m.ExternalLogins = (await signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
                if (ModelState.IsValid)
                {
                    if (!_validatorService.HasRequestValidCaptchaEntry(Language.English, DisplayMode.ShowDigits))
                    {
                        this.ModelState.AddModelError(_catpchaOptions.CaptchaComponent.CaptchaInputName, "Enter a valid key");
                        return View(m);
                    }
                    var result = await signInManager.PasswordSignInAsync(m.Email, m.Password, m.RememberMe, false);
                    if (result.Succeeded)
                    {
                        //session variable that contains user ID after login
                        var user = await userManager.FindByNameAsync(m.Email);
                        var userID = user.Id;
                        var userName = user.firstName + " " + user.lastName;
                        HttpContext.Session.SetString("userID", userID);
                        HttpContext.Session.SetString("userName", userName);
                        return RedirectToAction("Home", "Home");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Invalid Login Attempt");
                    }
                }
                return View(m);
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);
                return View(m);
            }
        }

        public IActionResult SignUp()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl)
        {
            try
            {
                LoginViewModel model = new LoginViewModel
                {
                    ReturnUrl = returnUrl,
                    ExternalLogins = (await signInManager.GetExternalAuthenticationSchemesAsync()).ToList(),
                    flag = con.GetSection("Flag").GetSection("FlagValue").Value,
            };

                return View(model);
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);
                return View();
            }

        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult ExternalLogin(string provider, string returnUrl)
        {
            try
            {
                var redirectUrl = Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl });

                var properties = signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);

                return new ChallengeResult(provider, properties);
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);
                return View();
            }
        }

        public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null, string remoteError = null)
        {
            try
            {
                returnUrl = returnUrl ?? Url.Content("~/");
                LoginViewModel loginViewModel = new LoginViewModel

                {
                    ReturnUrl = returnUrl,
                    ExternalLogins = (await signInManager.GetExternalAuthenticationSchemesAsync()).ToList()
                };

                if (remoteError != null)
                {
                    ModelState.AddModelError("", $"Error from external provider:{remoteError}");
                    return View("Login", loginViewModel);
                }


                var info = await signInManager.GetExternalLoginInfoAsync();

                if (info == null)
                {
                    {
                        ModelState.AddModelError("", "Error in loading external information");//for display use Validation.Summary in razorview
                        return View("Login", loginViewModel);
                    }
                }

                var signInResult = await signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false,
                    bypassTwoFactor: true);

                if (signInResult.Succeeded)
                {
                    var email = info.Principal.FindFirstValue(ClaimTypes.Email);
                    var loggedUser = await userManager.FindByEmailAsync(email);
                    var userID = loggedUser.Id;
                    var userName = loggedUser.firstName + " " + loggedUser.lastName;
                    HttpContext.Session.SetString("userID", userID);
                    HttpContext.Session.SetString("userName", userName);
                    return RedirectToAction("Home", "Home");
                }
                else
                {
                    var email = info.Principal.FindFirstValue(ClaimTypes.Email);
                    if (email != null)
                    {
                        var user = await userManager.FindByEmailAsync(email);
                        if (user == null)
                        {
                            user = new IdentityUserHelper
                            {
                                firstName = info.Principal.FindFirstValue(ClaimTypes.GivenName),
                                lastName = info.Principal.FindFirstValue(ClaimTypes.Surname),
                                //addressLine1 = info.Principal.FindFirstValue(ClaimTypes.StreetAddress),
                                //postcode = Convert.ToInt32(info.Principal.FindFirstValue(ClaimTypes.PostalCode)),
                                //state = info.Principal.FindFirstValue(ClaimTypes.StateOrProvince),
                                //country = info.Principal.FindFirstValue(ClaimTypes.Country),
                                UserName = info.Principal.FindFirstValue(ClaimTypes.Email),
                                Email = info.Principal.FindFirstValue(ClaimTypes.Email),
                                //PhoneNumber = info.Principal.FindFirstValue(ClaimTypes.MobilePhone)

                            };
                            //await userManager.CreateAsync(user);
                            return RedirectToAction("RegisterGF", user);
                        }

                        await userManager.AddLoginAsync(user, info);
                        await userManager.AddToRoleAsync(user, "User");
                        await signInManager.SignInAsync(user, isPersistent: false);
                        user = await userManager.FindByNameAsync(email);
                        var userID = user.Id;
                        var userName = user.firstName + " " + user.lastName;
                        HttpContext.Session.SetString("userID", userID);
                        HttpContext.Session.SetString("userName", userName);

                        return RedirectToAction("Home", "Home");
                    }
                }
                ViewBag.ErrorTitle = $"Email claim not received from: {info.LoginProvider}";
                ViewBag.ErrorMessage = "Please contact AVIV-IT support";
                return View("Error");
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);
                ViewBag.ErrorTitle = "Login atempd failed";
                ViewBag.ErrorMessage = "Please contact AVIV-IT support";
                return View("Error");
            }

        }
        public IActionResult Denied()
        {
            return View();
        }
        [HttpGet]
        public IActionResult RegisterGF(IdentityUserHelper user)
        {
            RegisterViewModel userReg = new RegisterViewModel
            {
                firstName = user.firstName,
                lastName = user.lastName,
                email = user.Email,
                phone = user.PhoneNumber,
                addressLine1 = user.addressLine1,
                suburb = user.suburb,
                postcode = user.postcode,
                state = user.state,
                country = user.country
            };


            return View(userReg);
        }
        [HttpPost]
        public async Task<IActionResult> RegisterGF(RegisterViewModel m)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    if (!_validatorService.HasRequestValidCaptchaEntry(Language.English, DisplayMode.ShowDigits))
                    {
                        this.ModelState.AddModelError(_catpchaOptions.CaptchaComponent.CaptchaInputName, "Enter a valid key");
                        return View(m);
                    }
                    var user = new IdentityUserHelper
                    {
                        UserName = m.email,
                        Email = m.email,
                        firstName = m.firstName,
                        lastName = m.lastName,
                        PhoneNumber = m.phone,
                        addressLine1 = m.addressLine1,
                        suburb = m.suburb,
                        postcode = (int)m.postcode,
                        state = m.state,
                        country = m.country
                    };
                    if (m.IsChecked == true)
                    {
                        var result = await userManager.CreateAsync(user, m.password);
                        result = await userManager.AddToRoleAsync(user, "User");
                        if (result.Succeeded)
                        {
                            await signInManager.SignInAsync(user, isPersistent: false);
                            return RedirectToAction("Home", "Home");
                        }
                        foreach (var e in result.Errors)
                        {
                            ModelState.AddModelError("", e.Description);
                        }
                    }
                    ModelState.AddModelError("isChecked", "You must read and accept Privacy Policy");
                }
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);
                return View();
            }

            return View(m);
        }
        public async Task<IActionResult> Logout()
        {
            await signInManager.SignOutAsync();
            return RedirectToAction("Login", "Account");
        }
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if(ModelState.IsValid)
            {
                var user = await userManager.FindByEmailAsync(model.Email);
                if(user != null)
                {
                    var token = await userManager.GeneratePasswordResetTokenAsync(user);
                    var passwordResetLink = Url.Action("ResetPassword", "Account",
                        new { email = model.Email, token = token }, Request.Scheme);

                    Email e = new Email();
                    e.toEmail = model.Email;
                    e.fromEmail = con.GetSection("Email").GetSection("FromEmail").Value;
                    e.title = "Password Reset Link ";
                    e.message = "Please click on the password reset link below to reset your password\n\n" + passwordResetLink;
                    sendEmail(e);
                    return View("ForgotPasswordConfirmation");
                }
                return View("ForgotPasswordConfirmation");
            }
            return View(model);
        }
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string token, string email)
        {
            if(token == null || email == null)
            {
                ModelState.AddModelError("", "Invalid password reset token");
            }
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if(ModelState.IsValid)
            {
                var user = await userManager.FindByEmailAsync(model.Email);
                if(user != null)
                {
                    var result = await userManager.ResetPasswordAsync(user, model.Token, model.Password);
                    if(result.Succeeded)
                    {
                        return View("ResetPasswordConfirmation");
                    }
                    foreach(var error in result.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                    }
                    return View(model);
                }
                return View("ResetPasswordConfirmation");
            }
            return View(model);
        }

        public void sendEmail(Email e)
        {
            string strKey = configuration.GetValue<string>("SendGridKey");
            EmailOp emailOp = new EmailOp(e.toEmail, e.fromEmail, e.title, e.message, strKey);
        }
    }

}
