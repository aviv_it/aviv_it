﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AVI_IT.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace AVI_IT.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdministrationController : Controller
    {
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly UserManager<IdentityUserHelper> userManager;
        List<string> userList;

        public AdministrationController(RoleManager<IdentityRole> roleManager, UserManager<IdentityUserHelper> userManager)
        {
            this.roleManager = roleManager;
            this.userManager = userManager;
            var users1 = userManager.Users;
            userList = new List<string>();
            foreach (var user in users1) userList.Add(user.Email);
        }
        [HttpGet]
        public IActionResult CreateRole()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateRole(CreateRoleViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IdentityRole identityRole = new IdentityRole
                    {
                        Name = model.RoleName
                    };
                    IdentityResult result = await roleManager.CreateAsync(identityRole);
                    if (result.Succeeded) return View("Display", roleManager.Roles);

                    foreach (IdentityError error in result.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                    }
                }
                return View("Display", roleManager.Roles);
            }
            catch(Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);
                return View("Display", roleManager.Roles);
            }
        }

        //[HttpGet]
        //public IActionResult ManageRole()
        //{
        //    ManageRoleViewModel mrole = new ManageRoleViewModel();
        //    FillArray(mrole);
        //    return View(mrole);
        //}
        //[HttpPost]
        //public async Task<IActionResult> ManageRole(ManageRoleViewModel mrole)
        //{
        //    var role = await roleManager.FindByIdAsync(mrole.roleID);
        //    var user = await userManager.FindByIdAsync(mrole.userID);
        //    if(null == role) return View("Not Found");
        //    if(!(await userManager.IsInRoleAsync(user, role.Name)))
        //    {
        //        var result = await userManager.AddToRoleAsync(user, role.Name);
        //    }

        //    return View("Display", roleManager.Roles);
        //}

        //private void FillArray(ManageRoleViewModel mrole)
        //{
        //    var users = userManager.Users;
        //    mrole.userList = new List<SelectListItem>();
        //    foreach (var user in users)
        //    {
        //        SelectListItem item = new SelectListItem();
        //        item.Text = user.UserName;
        //        item.Value = user.Id;
        //        mrole.userList.Add(item);
        //    }

        //    var roles = roleManager.Roles;
        //    mrole.roleList = new List<SelectListItem>();
        //    foreach (var role in roles)
        //    {
        //        SelectListItem item = new SelectListItem();
        //        item.Text = role.Name;
        //        item.Value = role.Id;
        //        mrole.roleList.Add(item);
        //    }

        //}
    }
}
