﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AVI_IT.Models;
using AVI_IT.ViewModels;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace AVI_IT.Controllers
{

    public class BlogController : Controller
    {
        private readonly AvivDataContext _db;
        private UserManager<IdentityUserHelper> userManager;
        public readonly IConfigurationRoot configuration;
        public BlogController(AvivDataContext db, IWebHostEnvironment env, UserManager<IdentityUserHelper> userManager)
        {
            _db = db;
            this.userManager = userManager;
            configuration = new ConfigurationBuilder()
                       .AddEnvironmentVariables()
                       .AddJsonFile(env.ContentRootPath + "/config.json")
                       .Build();
        }

       
        public IActionResult Index()
        {

            var Result = from b in _db.blog
                         join aspu in _db.Users on b.userID equals aspu.Id
                         select new BlogViewModel
                         {
                             blogID = b.blogID,
                             blogTitle = b.blogTitle,
                             blogContent = b.blogContent,
                             publishedDate = b.publishedDate,
                             firstName = aspu.firstName,
                             lastName = aspu.lastName,
                         };

            return View(Result);
        }

        public IActionResult ManageBlog()
        {


            var Result = from b in _db.blog
                         join aspu in _db.Users on b.userID equals aspu.Id
                         select new BlogViewModel
                         {
                             blogID = b.blogID,
                             blogTitle = b.blogTitle,

                             blogContent = b.blogContent,
                             publishedDate = b.publishedDate,

                             firstName = aspu.firstName,
                             lastName = aspu.lastName,
                         };

            return View(Result);
        }


        //Delete blog from list
       
        public ActionResult Delete(int Id)
        {
            LeaveCommentViewModel delet = new LeaveCommentViewModel();
            //must remove comments first, before blogID
            //var deleteBlogComments = (from c in _db.comment
            //                          where c.blogID == Id
            //                          select c);

            IEnumerable<LeaveCommentViewModel> list = _db.comment.Where(x => x.blogID == Id).ToList();
            _db.comment.RemoveRange(list);

            _db.SaveChanges();

            //if (deleteBlogComments != null)
            //{

            //    _db.comment.Remove(deleteBlogComments);
            //    _db.SaveChanges();
            //}


            var deleteBlogID = (from b in _db.blog
                                where b.blogID == Id
                                select b).FirstOrDefault();

            if (deleteBlogID != null)
            {

                _db.blog.Remove(deleteBlogID);
                _db.SaveChanges();
            }
            return RedirectToAction("ManageBlog");
        }


        [HttpGet]
        public IActionResult AddBlog()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AddBlog(BlogViewModel postBlog)
        {
            BlogViewModel sess = new BlogViewModel();

            //session variable passing userID
            sess.userID = HttpContext.Session.GetString("userID");

            // postBlog.userID = sess.userID; //add userID from session
            DateTime now = new DateTime();
            now = DateTime.Today;

            BlogViewModel blog2 = new BlogViewModel
            {
                firstName = postBlog.firstName,
                lastName = postBlog.lastName,
                blogID = postBlog.blogID,
                blogContent = postBlog.blogContent,
                blogTitle = postBlog.blogTitle,
                publishedDate = now,
                userID = sess.userID
            };
           

            _db.blog.Add(blog2);
            _db.SaveChanges();


            return RedirectToAction("ManageBlog");
        }
       /* public async Task<IActionResult> Display(int bp)
        {
            List<BlogViewModel> blog = new List<BlogViewModel>();
           
            try
            {
                
                var comments = from comm in _db.comment
                               where comm.blogID == bp
                               select new CommentModel
                               {
                                   commentID = comm.commentID,
                                   blogID = comm.blogID,
                                   userID = comm.userID,
                                   comment = comm.comment,
                                   reply = comm.reply,
                                 
                               };

                var blogResult = from a in _db.Users
                                 join b in _db.blog on a.Id equals b.userID
                                 select new BlogViewModel
                                 {
                                     blogID = b.blogID,
                                     blogTitle = b.blogTitle,
                                     blogContent = b.blogContent,
                                     userID = a.Id,
                                     publishedDate = b.publishedDate,
                                     firstName = a.firstName,
                                     lastName = a.lastName
                                 };

                 foreach (var item in blogResult)
                {
                    if (item.blogID == bp)
                    {
                        item.Comments = comments;
                        blog.Add(item);
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

            }


        }*/

        //post in leave comments
        [HttpPost]
        public IActionResult Display(LeaveCommentViewModel bp)
        {
            LeaveCommentViewModel lc = new LeaveCommentViewModel();

            lc.userID = HttpContext.Session.GetString("userID");
            lc.blogID = HttpContext.Session.GetInt32("blogID");
            lc.userName = HttpContext.Session.GetString("userName");

            bp.blogID = lc.blogID;
            bp.userID = lc.userID;
            bp.userName = lc.userName;

            _db.comment.Add(bp);
            _db.SaveChanges();

            return RedirectToAction("Index");
        }

        public IActionResult Display(int bp)
        {
            List<BlogViewModel> blog = new List<BlogViewModel>();


            var comments = from comm in _db.comment
                           where comm.blogID == bp
                           select new LeaveCommentViewModel
                           {
                               commentID = comm.commentID,
                               blogID = comm.blogID,
                               userID = comm.userID,
                               comment = comm.comment,
                               reply = comm.reply,
                               userName = comm.userName,
                           };

            var blogResult = from a in _db.Users
                             join b in _db.blog on a.Id equals b.userID
                             select new BlogViewModel
                             {
                                 blogID = b.blogID,
                                 blogTitle = b.blogTitle,
                                 blogContent = b.blogContent,
                                 userID = a.Id,
                                 publishedDate = b.publishedDate,
                                 firstName = a.firstName,
                                 lastName = a.lastName,
                                 //Comments = comments
                             };
            // List<Product> products = HttpContext.Session.Get<List<Product>>("products");
            // BlogViewModel blog2 = new BlogViewModel();

            HttpContext.Session.SetInt32("blogID", bp);

            foreach (var item in blogResult)
            {
                if (item.blogID == bp)
                {
                    item.Comments = comments;
                    blog.Add(item);

                }

            }

            return View(blog);
        }
    }
}
