﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AVI_IT.Models;
using AVI_IT.Services;
using AVI_IT.ViewModels;
using DNTCaptcha.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace AVI_IT.Controllers
{
    [Authorize]
    public class HelpDeskController : Controller
    {
        
        private readonly AvivDataContext _db;
        public readonly IConfigurationRoot configuration;
        public readonly IConfiguration con;
        private readonly IDNTCaptchaValidatorService _validatorService;
        private readonly DNTCaptchaOptions _catpchaOptions;
        private UserManager<IdentityUserHelper> userManager;
        private SignInManager<IdentityUserHelper> signInManager;
        public HelpDeskController(AvivDataContext db, IWebHostEnvironment env,
           IDNTCaptchaValidatorService validatorService, IOptions<DNTCaptchaOptions> catpchaOptions, IConfiguration con, UserManager<IdentityUserHelper> userManager)
        {
            _db = db;
            configuration = new ConfigurationBuilder()
                       .AddEnvironmentVariables()
                       .AddJsonFile(env.ContentRootPath + "/config.json")
                       .Build();
            this._validatorService = validatorService;
            this._catpchaOptions = catpchaOptions == null ? throw new ArgumentNullException(nameof(catpchaOptions)) :                                                                                          catpchaOptions.Value;
            this.con = con;
            this.userManager = userManager;
        }

       
        [HttpPost]
        public async Task<IActionResult> GetTicket(HelpDeskViewModel h)
        {
            
            HelpDeskViewModel sess = new HelpDeskViewModel();
            
            try
            {
                //session variable passing userID
                sess.userID = HttpContext.Session.GetString("userID");
                var user = await userManager.FindByIdAsync(sess.userID);

                if (!ModelState.IsValid) return View(h);
                else
                {
                    if (!_validatorService.HasRequestValidCaptchaEntry(Language.English, DisplayMode.ShowDigits))
                    {
                        this.ModelState.AddModelError(_catpchaOptions.CaptchaComponent.CaptchaInputName, "Enter a valid key");
                        return View(h);
                    }
                    //h.empID = null; //add ticket record without employeeID - ticket hasn't been assigned yet
                    h.userID = sess.userID; //add userID from session

                    _db.helpdesk.Add(h);
                    _db.SaveChanges();

                    //sending email to the client aftter quotation is sent
                    Email e = new Email();
                    e.toEmail = user.Email;
                    e.fromEmail = con.GetSection("Email").GetSection("FromEmail").Value;
                    e.title = "New Helpdesk ticket " + h.ticketID;
                    e.message = "Your Helpdesk ticket " + h.ticketID + " has been submited and it is under review ";
                    sendEmail(e);
                    //sending email to AVIV it after quotation is requested

                    Email e1 = new Email();
                    e1.toEmail = con.GetSection("Email").GetSection("FromEmail").Value;
                    e1.fromEmail = con.GetSection("Email").GetSection("FromEmail").Value;
                    e1.title = "New Helpdesk ticket " + h.ticketID + " from " + h.userID;
                    e1.message = "New Helpdesk ticket " + h.ticketID + " from " + h.userID
                        + "\nCategory: " + h.category
                        + "\nDescription: " + h.ticketDescription;
                    sendEmail(e1);
                }
                
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message); ;
            }
            return RedirectToAction("Feedback");
            
        }
      

        public IActionResult Feedback()
        {

            return View("Feedback");
        }


        //Front end
        [HttpGet]
        public IActionResult GetTicket()
        {

            HelpDeskViewModel helpDesk = new HelpDeskViewModel
            {
                ticketDescription = ""
            };
            try
            {
                FillArray(helpDesk);
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message); ;
            }
            return View(helpDesk);
        }

         private void FillArray(HelpDeskViewModel helpList)
        {
            SelectListItem item = new SelectListItem();
            item.Text = "HardWare";
            item.Value = "HardWare";

            helpList.categoryList.Add(item);

            item = new SelectListItem();
            item.Text = "Networking";
            item.Value = "Networking";

            helpList.categoryList.Add(item);

            item = new SelectListItem();
            item.Text = "Software Installation Issues";
            item.Value = "Software Installation Issues";

            helpList.categoryList.Add(item);

            item = new SelectListItem();
            item.Text = "Data Recovery";
            item.Value = "Data Recovery";

            helpList.categoryList.Add(item);
        }

        [HttpPost]
        public IActionResult Display()
        {
            //linq to join employee,user and helpdesk tables
            List<Employee> employees = _db.employee.ToList();
            List<IdentityUserHelper> users = _db.Users.ToList();
            List<HelpDeskViewModel> tickets = _db.helpdesk.ToList();
            var ticketsList = new List<HelpDeskViewModel>();
            try
            {
                var empUser = from e in employees
                              join u in users on e.userID equals u.Id into empUserTable
                              from u in empUserTable.ToList()
                              join t in tickets on e.employeeID equals t.empID into empUserTable2
                              from t in empUserTable2.ToList()
                              select new EmpUserViewModel
                              {
                                  employee = e,
                                  user = u,
                                  ticket = t
                              };

                foreach (var user in empUser)
                {
                    HelpDeskViewModel helpdesk = new HelpDeskViewModel();

                    helpdesk.ticketID = user.ticket.ticketID;
                    helpdesk.ticketDescription = user.ticket.ticketDescription;
                    helpdesk.empID = user.ticket.empID;
                    helpdesk.category = user.ticket.category;
                    helpdesk.userID = user.ticket.userID;
                    helpdesk.empName = user.user.firstName.ToString() + " " + user.user.lastName.ToString();
                    ticketsList.Add(helpdesk);
                }
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message); ;
            }
            
            return View(ticketsList.OrderBy(model => model.ticketID));
        }
        public void sendEmail(Email e)
        {
            string strKey = configuration.GetValue<string>("SendGridKey");
            EmailOp emailOp = new EmailOp(e.toEmail, e.fromEmail, e.title, e.message, strKey);
        }

    }
}
