﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AVI_IT.Models;
using AVI_IT.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace AVI_IT.Controllers
{
    [Authorize(Roles = "Admin, " +
                       "Employee")]
    public class AdminPageController : Controller
    {
        private readonly AvivDataContext _db;
        private readonly UserManager<IdentityUserHelper> userManager;

        public AdminPageController(AvivDataContext db, UserManager<IdentityUserHelper> userManager)
        {
            this.userManager = userManager;
            this._db = db;
        }


        public IActionResult Index()
        {
                return View();
        }
        [HttpGet]
        public IActionResult ManageTickets()
        {
            AdminPageViewModel thisViewModel = new AdminPageViewModel();
            try
            {
                
                var employeeList = new List<SelectListItem>();
                var dict = new Dictionary<int, List<SelectListItem>>();

                //linq to join employee and user tables
                List<Employee> employees = _db.employee.ToList();
                List<IdentityUserHelper> users = _db.Users.ToList();

                var empUser = from e in employees
                              join u in users on e.userID equals u.Id into empUserTable
                              from u in empUserTable.ToList()
                              select new EmpUserViewModel
                              {
                                  employee = e,
                                  user = u
                              };
                foreach (var user in empUser)
                {
                    SelectListItem item = new SelectListItem();
                    item.Value = user.employee.employeeID.ToString();
                    item.Text = user.user.firstName.ToString() + " " + user.user.lastName.ToString();
                    employeeList.Add(item);
                }

                /*foreach (Employee employee in _db.employee)
                {
                    SelectListItem item = new SelectListItem();
                    item.Value = employee.employeeID.ToString();
                    item.Text = employee.employeeID.ToString();
                    employeeList.Add(item);
                }*/
                foreach (HelpDeskViewModel helpDesk in _db.helpdesk)
                {
                    if (helpDesk.empID == null)
                    {
                        thisViewModel.dict = dict;
                        if (!dict.ContainsKey(helpDesk.ticketID))
                            thisViewModel.dict.Add(helpDesk.ticketID, employeeList);
                    }
                }
                return View(thisViewModel);
            }
            catch(Exception ex)
            {

                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);
                return View(thisViewModel);
            }
            

        }
        [HttpPost]
        public IActionResult ManageTickets(AdminPageViewModel thisViewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(thisViewModel);
                }
                else
                {
                    List<HelpDeskViewModel> list = new List<HelpDeskViewModel>();
                    //for helpdesk data handling
                    for (int i = 0; i < thisViewModel.selectedDict.Count; i++)
                    {
                        foreach (HelpDeskViewModel hd in _db.helpdesk)
                        {
                            if (thisViewModel.selectedDict.ElementAt(i).Key == hd.ticketID && thisViewModel.selectedDict.ElementAt(i).Value != null)
                            {
                                hd.ticketID = Convert.ToInt32(thisViewModel.selectedDict.ElementAt(i).Key);
                                hd.empID = Convert.ToInt32(thisViewModel.selectedDict.ElementAt(i).Value);
                                list.Add(hd);
                            }

                        }
                    }
                    foreach (var model in list)
                    {
                        _db.Entry(model).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                        _db.SaveChanges();
                    }

                }

            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message); ;
            }
            return RedirectToAction("ManageTickets");
        }
        [HttpGet]
        public IActionResult ManageQuotes()
        {
            AdminPageViewModel thisViewModel = new AdminPageViewModel();
            try
            {
                var employeeList = new List<SelectListItem>();
                var dictQuote = new Dictionary<int, List<SelectListItem>>();

                //linq to join employee and user tables
                List<Employee> employees = _db.employee.ToList();
                List<IdentityUserHelper> users = _db.Users.ToList();

                var empUser = from e in employees
                              join u in users on e.userID equals u.Id into empUserTable
                              from u in empUserTable.ToList()
                              select new EmpUserViewModel
                              {
                                  employee = e,
                                  user = u
                              };
                foreach (var user in empUser)
                {
                    SelectListItem item = new SelectListItem();
                    item.Value = user.employee.employeeID.ToString();
                    item.Text = user.user.firstName.ToString() + " " + user.user.lastName.ToString();
                    employeeList.Add(item);
                }

                foreach (Quotation q in _db.quotation)
                {
                    if (q.empID == null)
                    {
                        thisViewModel.dictQoute = dictQuote;
                        if (!dictQuote.ContainsKey(q.quoteID))
                            thisViewModel.dictQoute.Add(q.quoteID, employeeList);
                    }
                }
                
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message); ;
            }
            return View(thisViewModel);

        }
        [HttpPost]
        public IActionResult ManageQuotes(AdminPageViewModel thisViewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(thisViewModel);
                }
                else
                {
                    List<Quotation> listQuote = new List<Quotation>();
                    //for Quotation data handling
                    for (int i = 0; i < thisViewModel.selectedDictQuote.Count; i++)
                    {
                        foreach (Quotation q in _db.quotation)
                        {
                            if (thisViewModel.selectedDictQuote.ElementAt(i).Key == q.quoteID && thisViewModel.selectedDictQuote.ElementAt(i).Value != null)
                            {
                                q.quoteID = Convert.ToInt32(thisViewModel.selectedDictQuote.ElementAt(i).Key);
                                q.empID = Convert.ToInt32(thisViewModel.selectedDictQuote.ElementAt(i).Value);
                                listQuote.Add(q);
                            }

                        }
                    }
                    foreach (var model in listQuote)
                    {
                        _db.Entry(model).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                        _db.SaveChanges();
                    }
                }

            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message); ;
            }

            return RedirectToAction("ManageQuotes");
        }

        public IActionResult ManageService()
        {
            List<Service> ser = new List<Service>();
            try
            {
                
                var Result = from v in _db.service
                             select v;

                foreach (var i in Result)
                {
                    ser.Add(new Service
                    {
                        serviceID = i.serviceID,
                        serviceTitle = i.serviceTitle,
                        serviceContent = i.serviceContent
                    });

                }

            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message); ;
            }
            return View(ser);

        }
       

        
        public ActionResult DeleteService(int Id)
        {
            //using linq to delete the record
            var delObj = _db.service.Where(q => q.serviceID == Id).FirstOrDefault();
            _db.service.Remove(delObj);
            _db.SaveChanges();
            return RedirectToAction("ManageService");
        }
    }

}
