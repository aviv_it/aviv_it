﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AVI_IT.Models;
using Microsoft.AspNetCore.Mvc;

namespace AVI_IT.Controllers
{

    public class HomeController : Controller
    {
        /* public IActionResult Service()
         {
             return View();
         }
 */
        private readonly AvivDataContext _db;

        public HomeController(AvivDataContext db)
        {
            _db = db;
        }

      /*  public HomeController()
        {
        }*/

        public IActionResult Home()
        {
            return View();
        }
        [HttpGet]
        public IActionResult Service()
        {
            List<Service> ser = new List<Service>();
            var Result = from v in _db.service
                         select v;
          
                foreach (var i in Result)
                {
                    ser.Add(new Service
                    {
                        serviceID = i.serviceID,
                        serviceTitle = i.serviceTitle,
                        serviceContent = i.serviceContent
                    });

                }

            return View(ser);
        }

        [HttpGet]
        public IActionResult EditService(int? ID)
        {
           /* var p = from v in _db.service
                    where v.serviceID == ID
                    select v;*/
             Service s = _db.service.Find(ID);
            //Console.WriteLine(p.serviceID);
            return View(s);
        }

        [HttpPost]
        public async Task<IActionResult> EditService(Service p)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View(p);

                _db.Update(p);
                await _db.SaveChangesAsync();
              
                return RedirectToAction("ManageService", "AdminPage");
            }
            catch (Exception ex)
            {

                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);
                return View(p);
            }
            
        }

        [HttpGet]
        public IActionResult AddService()
        {
            Service s = new Service();
          
            return View(s);
        }

        [HttpPost]
        public IActionResult AddService(Service? s)
        {

                
                if (!ModelState.IsValid) return View(s);
                if(s != null)
                {
                    _db.service?.Add(s);
                    _db?.SaveChanges();

                    Service[] s1 = _db.service?.ToArray();
                    return RedirectToAction("ManageService","AdminPage", s1);
                }
                else
                {
                    return View(s);
                }
               
           
            
        }
    }
}
