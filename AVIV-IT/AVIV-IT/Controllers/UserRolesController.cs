﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AVI_IT.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using AVI_IT.Models;

namespace AVI_IT.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UserRolesController : Controller
    {
        private readonly UserManager<IdentityUserHelper> userManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly AvivDataContext _db;

        public UserRolesController(UserManager<IdentityUserHelper> userManager, RoleManager<IdentityRole> roleManager, AvivDataContext db)
        {
            this.roleManager = roleManager;
            this.userManager = userManager;
            _db = db;
        }
        public async Task<IActionResult> Index()
        {
            try
            {
                var users = await userManager.Users.ToListAsync();
                var userRolesViewModel = new List<UserRolesViewModel>();
                /*var id = HttpContext.Session.GetString("userID");
                var userLogged = await userManager.FindByIdAsync(id);
                var roles = userManager.GetRolesAsync(userLogged);*/

                foreach (IdentityUserHelper user in users)
                {
                    var thisViewModel = new UserRolesViewModel();
                    thisViewModel.UserId = user.Id;
                    thisViewModel.Email = user.Email;
                    thisViewModel.FirstName = user.firstName;
                    thisViewModel.LastName = user.lastName;
                    thisViewModel.Roles = await GetUserRoles(user);
                    userRolesViewModel.Add(thisViewModel);
                }
                return View(userRolesViewModel);
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);
                return View();
            }
           
        }
        private async Task<List<string>> GetUserRoles(IdentityUserHelper user)
        {
            return new List<string>(await userManager.GetRolesAsync(user));
        }

        public async Task<IActionResult> Manage(string userId, Employee employee)
        {
            try
            {
                var id = HttpContext.Session.GetString("userID");
                var userLogged = await userManager.FindByIdAsync(id);
                if (userLogged != null)
                {
                    var roles = userManager.GetRolesAsync(userLogged);
                    //if (roles.Result != null && roles.Result.Contains("Admin"))
                    //{
                    ViewBag.userId = userId;
                    var user = await userManager.FindByIdAsync(userId);

                    if (user == null)
                    {
                        ViewBag.ErrorMessage = $"User with Id = {userId} cannot be found";
                        return View("NotFound");
                    }
                    ViewBag.UserName = user.UserName;
                    var model = new List<ManageUserRolesViewModel>();
                    foreach (var role in roleManager.Roles.ToList())
                    {
                        var userRolesViewModel = new ManageUserRolesViewModel
                        {
                            RoleId = role.Id,
                            RoleName = role.Name
                        };
                        if (await userManager.IsInRoleAsync(user, role.Name))
                        {
                            userRolesViewModel.Selected = true;
                        }
                        else
                        {
                            userRolesViewModel.Selected = false;
                        }
                        model.Add(userRolesViewModel);
                    }
                    employee.userID = userId;
                    return View(model);
                }
                else
                    return RedirectToAction("Login", "Account");
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);
                return View();
            }

        }
        [HttpPost]
        public async Task<IActionResult> Manage(List<ManageUserRolesViewModel> model, string userId, Employee employee)
        {
            try
            {
                var user = await userManager.FindByIdAsync(userId);

                if (user == null)
                {
                    return View();
                }
                var roles = await userManager.GetRolesAsync(user);
                var result = await userManager.RemoveFromRolesAsync(user, roles);

                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", "Cannot remove user existing roles");
                    return View(model);
                }
                //checks if user is not an employee
                if (!roles.Contains("Employee"))
                {
                    //adds selected role for the user
                    result = await userManager.AddToRolesAsync(user, model.Where(x => x.Selected).Select(y => y.RoleName));
                    //checks if selected role was Employee, if yes adds to the database
                    if (await userManager.IsInRoleAsync(user, "Employee"))
                    {
                        _db.employee.Add(employee);
                        _db.SaveChanges();
                    }
                }
                else
                {
                    result = await userManager.AddToRolesAsync(user, model.Where(x => x.Selected).Select(y => y.RoleName));
                    if (await userManager.IsInRoleAsync(user, "Employee") != true)
                    {
                        //code for deleting emplyeeID from employee table
                        var delObj = _db.employee.Where(q => q.userID == employee.userID).FirstOrDefault();

                        _db.employee.Remove(delObj);
                        _db.SaveChanges();

                    }

                }


                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", "Cannot add selected roles to user");
                    return View(model);
                }

                return RedirectToAction("Index");


            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);
                return View();
            }
        }
            
    }
}
