﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AVI_IT.Models;
using AVI_IT.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using NLog;
using static AVI_IT.ViewModels.PriceRangeViewModel;

namespace AVI_IT.Controllers
{
    [Authorize(Roles = "Admin, " +
                       "Employee")]
    public class ProductController : Controller
    {
        private readonly AvivDataContext _db;
        private readonly IWebHostEnvironment _he;

        public ProductController(AvivDataContext db, IWebHostEnvironment he)
        {
            this._db = db;
            this._he = he;
        }
        public IActionResult Index()
        {
            List<Product> lProd = getList();
            return View(lProd);
        }

        public IActionResult Create()
        {
            ViewData["typeID"] = new SelectList(_db.product_Type.ToList(), "typeID", "type");
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(Product product, IFormFile image)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (image != null)
                    {
                        var name = Path.Combine(_he.WebRootPath + "/images", Path.GetFileName(image.FileName));
                        await image.CopyToAsync(new FileStream(name, FileMode.Create));
                        product.image = "images/" + image.FileName;
                    }
                    if (image == null)
                    {
                        product.image = "images/imgNotFound.PNG";
                    }
                    _db.product.Add(product);
                    await _db.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);

            }
            return RedirectToAction(nameof(Create));

        }
        public IActionResult Edit(int? id)
        {
            ViewData["typeID"] = new SelectList(_db.product_Type.ToList(), "typeID", "type");
            if (id == null)
            {
                return NotFound();
            }
            var product = _db.product.Include(m => m.productType).FirstOrDefault(m => m.productID == id);
            if (product == null)
            {
                return NotFound();
            }
            return View(product);
        }

        [HttpPost]
        public async Task <IActionResult> Edit (Product product, IFormFile image)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (image != null)
                    {
                        var name = Path.Combine(_he.WebRootPath + "/images", Path.GetFileName(image.FileName));
                        await image.CopyToAsync(new FileStream(name, FileMode.Create));
                        product.image = "images/" + image.FileName;
                    }
                    if (image == null)
                    {
                        product.image = "images/imgNotFound.PNG";
                    }
                    _db.Update(product);
                    await _db.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message);

            }
            return View(product);
        }
        public IActionResult Details(int? id)
        {
            if(id == null)
            {
                return NotFound();
            }
            List<Product> lp = getList();
            foreach(var item in lp)
            {
                if(item.productID == id)
                {
                    return View(item);
                }
            }
            return NotFound();
        }
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            List<Product> lp = getList();
            foreach (var item in lp)
            {
                if (item.productID == id)
                {
                    return View(item);
                }
            }
            return NotFound();
        }
        [HttpPost]
        [ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirm(int? id)
        {
            if(id == null)
            {
                return NotFound();
            }
            var product = _db.product.FirstOrDefault(m => m.productID == id);
            if(product == null)
            {
                return NotFound();
            }
            _db.product.Remove(product);
            await _db.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        //method to join tables product and product_type
        public List<Product> getList()
        {
            PriceRange[] proRangeArray = {new PriceRange { range="0-$50", bRange=false},
            new PriceRange { range="$50-$100", bRange=false},
            new PriceRange { range="More than $100", bRange=false}};


            List<Product> products = _db.product.ToList();
            List<ProductType> pTypes = _db.product_Type.ToList();
            var listP = new List<Product>();

            var prodWithTypes = from p in products
                                join t in pTypes on p.typeID equals t.typeID
                                select new ProdTypeViewModel
                                {
                                    product = p,
                                    pType = t
                                };
            foreach (var item in prodWithTypes)
            {
                Product p = new Product();
                p.productID = item.product.productID;
                p.productName = item.product.productName;
                p.typeID = item.product.typeID;
                p.productDes = item.product.productDes;
                p.stock = item.product.stock;
                p.price = item.product.price;
                p.image = item.product.image;
                if (p.price <= 50)
                {
                    p.proRange = new PriceRange { range = "0-$50", bRange = false };
                }
                else if (p.price > 50 && p.price <= 100)
                {
                    p.proRange = new PriceRange { range = "50-$100", bRange = false };
                }
                else
                {
                    p.proRange = new PriceRange { range = "More than $100", bRange = false };
                }
                p.productType = item.pType;
                p.ptList = pTypes;
                p.proRangeArray = proRangeArray;

                listP.Add(p);
            }
            return listP;
        }
    }
}
