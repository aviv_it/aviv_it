﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AVI_IT.Models;
using AVI_IT.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace AVI_IT.Controllers
{
    public class CommController : Controller
    {
        public readonly IConfigurationRoot configuration;
        public readonly IConfiguration con;
        public CommController comm { get; set; }
        public CommController(IWebHostEnvironment env, IConfiguration con)
        {
            configuration = new ConfigurationBuilder()
                        .AddEnvironmentVariables()
                        .AddJsonFile(env.ContentRootPath + "/config.json")
                        .Build();
            this.con = con;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public IActionResult Email()
        {
            Email e = new Email();
            try
            {
                e.toEmail = con.GetSection("Email").GetSection("FromEmail").Value;

            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message); ;
            }
            
            return View(e);
        }
        [HttpPost]
        public IActionResult Email(Email e)
        {
            try
            {
                sendEmail(e);
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message); ;
            }
            
            return RedirectToAction("Result");
        }
        public IActionResult Result()
        {
            return View();
        }
        public void sendEmail(Email e)
        {
            try
            {
                string strKey = configuration.GetValue<string>("SendGridKey");
                e.toEmail = con.GetSection("Email").GetSection("FromEmail").Value;
                EmailOp emailOp = new EmailOp(e.toEmail, e.fromEmail, e.title, e.message, strKey);
            }
            catch (Exception ex)
            {
                global.gLogger.log.Debug("Debug message: ", ex.Message);
                global.gLogger.log.Error(new Exception(), ex.Message);
                global.gLogger.log.Error(new Exception(), ex.StackTrace);
                global.gLogger.log.Fatal("Fatal message: ", ex.Message); ;
            }
            
        }
    }
}
