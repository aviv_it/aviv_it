#pragma checksum "C:\AVIV-IT\AVIV-IT\AVIV-IT\Views\AdminPage\ManageService.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "c9b8cfe51a6652dcc608155f41060a225aa8f959"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_AdminPage_ManageService), @"mvc.1.0.view", @"/Views/AdminPage/ManageService.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c9b8cfe51a6652dcc608155f41060a225aa8f959", @"/Views/AdminPage/ManageService.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"582ce889cbb70c404b228416915dda5a0b947950", @"/Views/_ViewImports.cshtml")]
    public class Views_AdminPage_ManageService : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<AVI_IT.Models.Service>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/javascript/Delete.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\n<div class=\"container-sm\">\n    <div class=\"row py-2\">\n        <div class=\"col-4 \">\n            <button class=\" btn btn-primary btn-lg form-control\">");
#nullable restore
#line 6 "C:\AVIV-IT\AVIV-IT\AVIV-IT\Views\AdminPage\ManageService.cshtml"
                                                            Write(Html.ActionLink("AddService", "AddService", "Home"));

#line default
#line hidden
#nullable disable
            WriteLiteral("</button>\n        </div>\n        <br />\n    </div>\n\n\n    <fieldset>\n");
#nullable restore
#line 13 "C:\AVIV-IT\AVIV-IT\AVIV-IT\Views\AdminPage\ManageService.cshtml"
         using (Html.BeginForm())
        {


#line default
#line hidden
#nullable disable
            WriteLiteral(@"            <table class=""table table-striped table-dark"">
                <thead>
                    <tr>
                        <th>Tile</th>
                        <th>Content</th>
                        <th></th>
                        <th></th>
                    </tr>

                </thead>
                <tbody>
");
#nullable restore
#line 27 "C:\AVIV-IT\AVIV-IT\AVIV-IT\Views\AdminPage\ManageService.cshtml"
                     foreach (var item in Model)
                    {

#line default
#line hidden
#nullable disable
            WriteLiteral("                        <tr>\n                            ");
#nullable restore
#line 30 "C:\AVIV-IT\AVIV-IT\AVIV-IT\Views\AdminPage\ManageService.cshtml"
                       Write(Html.HiddenFor(m => item.serviceID));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n                            <td>\n                                ");
#nullable restore
#line 32 "C:\AVIV-IT\AVIV-IT\AVIV-IT\Views\AdminPage\ManageService.cshtml"
                           Write(item.serviceTitle);

#line default
#line hidden
#nullable disable
            WriteLiteral("\n                            </td>\n                            <td>");
#nullable restore
#line 34 "C:\AVIV-IT\AVIV-IT\AVIV-IT\Views\AdminPage\ManageService.cshtml"
                           Write(item.serviceContent);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\n                            <td> <button class=\" btn btn-warning form-control \"> \n                                \n                                ");
#nullable restore
#line 37 "C:\AVIV-IT\AVIV-IT\AVIV-IT\Views\AdminPage\ManageService.cshtml"
                           Write(Html.ActionLink("Edit", "EditService", "Home", new { @id = item.serviceID}));

#line default
#line hidden
#nullable disable
            WriteLiteral("</button></td>\n                            <td>\n                                <a class=\"btn btn-danger delete\"\n                                   data-id=\"");
#nullable restore
#line 40 "C:\AVIV-IT\AVIV-IT\AVIV-IT\Views\AdminPage\ManageService.cshtml"
                                       Write(item.serviceID);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"""
                                   data-controller=""AdminPage""
                                   data-action=""DeleteService""
                                   data-body-message=""Are you sure to delete this service?"">
                                    Delete
                                </a>
                                <!-- Button trigger modal -->
                                <!--<button type=""button"" class=""btn btn-danger"" data-toggle=""modal"" data-target=""#deleteServiceModal"">
                                    Delete
                                </button>-->

                                <!-- Modal -->
                                <!--<div class=""modal fade"" id=""deleteServiceModal"" tabindex=""-1"" role=""dialog"" aria-labelledby=""deleteServiceModalLabel"" aria-hidden=""true"">
                                    <div class=""modal-dialog"" role=""document"">
                                        <div class=""modal-content"">
                                            <div class=""modal-header");
            WriteLiteral(@""">
                                                <h5 class=""modal-title"" id=""deleteServiceModalLabel"">Delete Service</h5>
                                                <button type=""button"" class=""close"" data-dismiss=""modal"" aria-label=""Close"">
                                                    <span aria-hidden=""true"">&times;</span>
                                                </button>
                                            </div>
                                            <div class=""modal-body"">
                                                Are you sure to delete this service?
                                            </div>
                                            <div class=""modal-footer"">
                                                <button type=""button"" class=""btn btn-secondary"" data-dismiss=""modal"">Close</button>
                                                <button type=""button"" class=""btn btn-danger"">
                                                    ");
#nullable restore
#line 67 "C:\AVIV-IT\AVIV-IT\AVIV-IT\Views\AdminPage\ManageService.cshtml"
                                               Write(Html.ActionLink("Delete", "DeleteService", "AdminPage", new { @Id = @item.serviceID }));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->
                            </td>

                        </tr>
");
#nullable restore
#line 76 "C:\AVIV-IT\AVIV-IT\AVIV-IT\Views\AdminPage\ManageService.cshtml"
                    }

#line default
#line hidden
#nullable disable
            WriteLiteral("                </tbody>\n\n\n            </table>\n");
#nullable restore
#line 81 "C:\AVIV-IT\AVIV-IT\AVIV-IT\Views\AdminPage\ManageService.cshtml"
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("    </fieldset>\n</div>\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "c9b8cfe51a6652dcc608155f41060a225aa8f9599438", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\n   \n\n\n\n\n\n\n\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<AVI_IT.Models.Service>> Html { get; private set; }
    }
}
#pragma warning restore 1591
