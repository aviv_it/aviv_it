#pragma checksum "C:\AVIV-IT\AVIV-IT\AVIV-IT\Views\Blog\ManageBlog.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "da4704984c909c9e0a7a8a5ec1d5a23feb65a6b2"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Blog_ManageBlog), @"mvc.1.0.view", @"/Views/Blog/ManageBlog.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"da4704984c909c9e0a7a8a5ec1d5a23feb65a6b2", @"/Views/Blog/ManageBlog.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"582ce889cbb70c404b228416915dda5a0b947950", @"/Views/_ViewImports.cshtml")]
    public class Views_Blog_ManageBlog : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<AVI_IT.ViewModels.BlogViewModel>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/javascript/Delete.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"
<style>
    #sortable tr:hover {
        background: #2c698d;
        color: #fff;
    }

</style>


<div class=""container"">
    <h3 class=""heading3"">All Blog Posts</h3>
    <div class=""card text-center p-2"">
        <div class=""card-body bg-transparent"">
            

            <table class=""table"">
                <thead class=""thead-dark"">
                    <tr>
                        <th scope=""col"">#</th>
                        <th scope=""col"">Blog Title</th>
                        <th scope=""col"">Date Created</th>
                        <th scope=""col"">Author</th>
                        <th scope=""col""></th>
                        <th scope=""col""></th>
                    </tr>
                </thead>
                <tbody id=""sortable"" style=""cursor:pointer;"">
");
#nullable restore
#line 31 "C:\AVIV-IT\AVIV-IT\AVIV-IT\Views\Blog\ManageBlog.cshtml"
                     foreach (var v in Model)
                    {

#line default
#line hidden
#nullable disable
            WriteLiteral("                        <tr >\n                            <td");
            BeginWriteAttribute("onclick", "  onclick=\"", 1027, "\"", 1112, 5);
            WriteAttributeValue("", 1038, "location.href", 1038, 13, true);
            WriteAttributeValue(" ", 1051, "=", 1052, 2, true);
            WriteAttributeValue(" ", 1053, "\'", 1054, 2, true);
#nullable restore
#line 34 "C:\AVIV-IT\AVIV-IT\AVIV-IT\Views\Blog\ManageBlog.cshtml"
WriteAttributeValue("", 1055, Url.Action("Display", "Blog", new { @bp = v.blogID }), 1055, 56, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 1111, "\'", 1111, 1, true);
            EndWriteAttribute();
            WriteLiteral(">");
#nullable restore
#line 34 "C:\AVIV-IT\AVIV-IT\AVIV-IT\Views\Blog\ManageBlog.cshtml"
                                                                                                                 Write(v.blogID);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\n                            <td>");
#nullable restore
#line 35 "C:\AVIV-IT\AVIV-IT\AVIV-IT\Views\Blog\ManageBlog.cshtml"
                           Write(v.blogTitle);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\n                            <td>");
#nullable restore
#line 36 "C:\AVIV-IT\AVIV-IT\AVIV-IT\Views\Blog\ManageBlog.cshtml"
                           Write(v.publishedDate.ToShortDateString());

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\n                            <td>");
#nullable restore
#line 37 "C:\AVIV-IT\AVIV-IT\AVIV-IT\Views\Blog\ManageBlog.cshtml"
                            Write(v.firstName + " " + v.lastName);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\n");
            WriteLiteral("                        <td>\n                            <a class=\"btn btn-danger delete\"\n                               data-id=\"");
#nullable restore
#line 41 "C:\AVIV-IT\AVIV-IT\AVIV-IT\Views\Blog\ManageBlog.cshtml"
                                   Write(v.blogID);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"""
                               data-controller=""Blog""
                               data-action=""Delete""
                               data-body-message=""Are you sure to delete this blog?"">
                                Delete
                            </a>
                          
");
            WriteLiteral("                        </td>\n                         \n                        </tr>\n");
#nullable restore
#line 52 "C:\AVIV-IT\AVIV-IT\AVIV-IT\Views\Blog\ManageBlog.cshtml"
                    }

#line default
#line hidden
#nullable disable
            WriteLiteral("                </tbody>\n            </table>\n\n\n            <button type=\"button\" class=\"btn btn-outline-info\">");
#nullable restore
#line 57 "C:\AVIV-IT\AVIV-IT\AVIV-IT\Views\Blog\ManageBlog.cshtml"
                                                          Write(Html.ActionLink("Add Blog", "AddBlog", "Blog"));

#line default
#line hidden
#nullable disable
            WriteLiteral("</button>\n        </div>\n        <div class=\"card-footer text-muted \">\n");
            WriteLiteral("        </div>\n    </div>\n\n</div>\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "da4704984c909c9e0a7a8a5ec1d5a23feb65a6b27552", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<AVI_IT.ViewModels.BlogViewModel>> Html { get; private set; }
    }
}
#pragma warning restore 1591
