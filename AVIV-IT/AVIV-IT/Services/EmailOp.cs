﻿using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AVI_IT.Services
{
    public class EmailOp
    {
        public EmailOp(string to, string from, string title, string message, string strKey)
        {
            try
            {
                var apiKey = strKey;
                var client = new SendGridClient(apiKey);
                var from_1 = new EmailAddress(from, "AVIV IT");
                var to_1 = new EmailAddress(to, "To email");
                var plainTextContent = message;
                var htmlContent = "<strong>" + message + "</strong>";
                var msg = MailHelper.CreateSingleEmail(from_1, to_1, title, plainTextContent, htmlContent);
                var response = client.SendEmailAsync(msg);//check the response if there is an issue log it
            }
            catch(Exception ex)
            {

            }
        }
    }
}
